package dao;

import java.util.List;

import model.Book;
import model.Message;
import model.Order;




import model.User;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class OrderDao {
	
    public void saveOrder(int uidin,int uidout,int bid,String otime,int ostatusin,int ostatusout){

    	
    	Configuration conf = new Configuration().configure();
    	SessionFactory sf = conf.buildSessionFactory();
    	Session sess = sf.openSession();
    	Transaction tx = sess.beginTransaction();
    		
	Order order=new Order();
		
	order.setBid(bid);
	order.setUidin(uidin);
	order.setUidout(uidout);
	order.setOtime(otime);
	order.setOstatusin(ostatusin);
	order.setOstatusout(ostatusout);
	sess.save(order);
	tx.commit();
	sess.close();
	
    }
    
    public void saveMessage(int uidsent,int uidrece,String mcontent,String mtime,int mstatus){
    	    	
    	Configuration conf = new Configuration().configure();
    	SessionFactory sf = conf.buildSessionFactory();
    	Session sess = sf.openSession();
    	Transaction tx = sess.beginTransaction();
    	
    	Message message=new Message();
    	message.setUidsent(uidsent);
    	message.setUidrece(uidrece);
    	message.setMcontent(mcontent);
    	message.setMtime(mtime);
    	message.setMstatus(mstatus);
    	sess.save(message);
    	tx.commit();
    	sess.close();
    	
    }

    public void update(int bid){

    	
    	Configuration conf = new Configuration().configure();
    	SessionFactory sf = conf.buildSessionFactory();
    	Session sess = sf.openSession();
    	Transaction tx = sess.beginTransaction();
    	
	Query query = null;
	List<Book> list = null;
		
	String hql = "select b from Book b where b.bid='"+bid+"'";	
	query = sess.createQuery(hql);	
	list = query.list();
	
	list.get(0).setBstatus(1);
	
	tx.commit();
	sess.close();
	
    }
    
    public int query(int bid){

    	
    	Configuration conf = new Configuration().configure();
    	SessionFactory sf = conf.buildSessionFactory();
    	Session sess = sf.openSession();
    	Transaction tx = sess.beginTransaction();
    	
    	Query query = null;
    	List<Book> list = null;
    	
    	String hql = "select n from Book n where n.bid='"+bid+"'";	
    	query = sess.createQuery(hql);	
    	list = query.list();
    	
    	int UidOut=list.get(0).getUid();
    	
    	tx.commit();
    	sess.close();
    	
    	return UidOut;
    }
    
    public String query2(int bid){
    	
    	Configuration conf = new Configuration().configure();
    	SessionFactory sf = conf.buildSessionFactory();
    	Session sess = sf.openSession();
    	Transaction tx = sess.beginTransaction();
    	
    	Query query = null;
    	List<Book> list = null;
    	
    	String hql = "select n from Book n where n.bid='"+bid+"'";	
    	query = sess.createQuery(hql);	
    	list = query.list();
    	
    	String bname=list.get(0).getBname();
    	
    	tx.commit();
    	sess.close();
    	
    	return bname;
    }
    
public String query3(int uidin){
    	
    	Configuration conf = new Configuration().configure();
    	SessionFactory sf = conf.buildSessionFactory();
    	Session sess = sf.openSession();
    	Transaction tx = sess.beginTransaction();
    	
    	Query query = null;
    	List<User> list = null;
    	
    	String hql = "select n from User n where n.uid='"+uidin+"'";	
    	query = sess.createQuery(hql);	
    	list = query.list();
    	
    	String uname=list.get(0).getUname();
    	
    	tx.commit();
    	sess.close();
    	
    	return uname;
    }

}
