package service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import dao.OrderDao;
import action.OrderAction;

public class OrderService {
	
	   
    Integer uidin;     //借这本书的用户id
    Integer uidout;    //发布这本书的用户id
    Integer bid;
    String otime;
    Integer ostatusin;   //
    Integer ostatusout;   //	   
	String turn;
    String mcontent;
    Integer mstatus;
    String bname;
   	
    
    public void os(Integer bid,Integer uidin,Integer uidout){	
    	
    HttpServletRequest request=ServletActionContext.getRequest();	
	
    OrderAction oa=new OrderAction();
    OrderDao od=new OrderDao();
    
    Date date=new Date();
    DateFormat format=new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");


	otime=format.format(date);
	ostatusin=2;
	ostatusout=2;
	mcontent=od.query3(uidin)+"想求购您发布的《"+od.query2(bid)+"》书";
	mstatus=0;
				
	od.saveOrder(uidin,uidout,bid,otime,ostatusin,ostatusout);
	od.saveMessage(uidin,uidout,mcontent,otime,mstatus);
	od.update(bid);	
	
	request.setAttribute("msg", "预定成功");
	
	turn="success";
	
    }
    
	public Integer getUidin() {
		return uidin;
	}
	public void setUidin(Integer uidin) {
		this.uidin = uidin;
	}
	public Integer getUidout() {
		return uidout;
	}
	public void setUidout(Integer uidout) {
		this.uidout = uidout;
	}
	public Integer getBid() {
		return bid;
	}
	public void setBid(Integer bid) {
		this.bid = bid;
	}
	public String getOtime() {
		return otime;
	}
	public void setOtime(String otime) {
		this.otime = otime;
	}
	public Integer getOstatusin() {
		return ostatusin;
	}
	public void setOstatusin(Integer ostatusin) {
		this.ostatusin = ostatusin;
	}
	public Integer getOstatusout() {
		return ostatusout;
	}
	public void setOstatusout(Integer ostatusout) {
		this.ostatusout = ostatusout;
	}
	
	 public String getTurn() {
			return turn;
		}

     public void setTurn(String turn) {
			this.turn = turn;
		}
	
}