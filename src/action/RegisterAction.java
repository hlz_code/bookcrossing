package action;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;




import utils.MD5;

public class RegisterAction {

	private String name;
	private String passwd;
	private String phone;
	

	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	
	
@SuppressWarnings("static-access")
public String execute(){
		
	    String turn;
	    HttpServletRequest request=ServletActionContext.getRequest();
		Configuration conf = new Configuration().configure();
		SessionFactory sf = conf.buildSessionFactory();
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		User user=new User();
		MD5 a=new MD5();
		
		
		String hql = "select n from User n where n.uname='"+getName()+"'";
		Query query = null;
		query = sess.createQuery(hql);
		List<User> list = null;
		list = query.list();
		
		if(!list.isEmpty()){
			tx.commit();
			sess.close();
			request.setAttribute("msg", "该账号已被注册！");
			turn="fail";
		}
		else{
			user.setUname(getName());
			user.setUnickname(getName());
			System.out.println(getName());
			user.setUpasswd(a.GetMD5Code(getPasswd()));
			user.setUpic("images/header/default-header.png");
			user.setUphone(getPhone());
			user.setUflag(1);
			sess.save(user);
			tx.commit();
			sess.close();
			request.setAttribute("msg", "注册成功！");
			request.setAttribute("url", "login.jsp?backurl=");
			turn="success";
		}
		
		return turn;
  }
}
