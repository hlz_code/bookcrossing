package action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import model.Book;
import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;

import sessionfactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PersonPublishAction extends ActionSupport{
	
	@Override
	public String execute() throws Exception {
		String name = null;
		HttpServletRequest request = ServletActionContext.getRequest();
		name = (String)request.getSession().getAttribute("username");
		String uhql = "select u from User u where u.uname='"+name+"'";
		name = (String)request.getSession().getAttribute("username");
		Session session=HibernateSessionFactory.getSession();					
		System.out.println(uhql);
		Query query=session.createQuery(uhql);
		List<User> ulist=query.list();
		int uid  = ulist.get(0).getUid();
		String bhql = "select b from Book b where b.uid='"+uid+"'";
		Query query2=session.createQuery(bhql);
		List<Book> list = session.createQuery(bhql).list();
		System.out.println(list);
		ActionContext.getContext().put("list", ulist);
		ActionContext.getContext().put("Book", list);
		return "success";
		
	}
	
}
