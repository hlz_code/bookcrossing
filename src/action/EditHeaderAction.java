package action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.hibernate.*;

import sessionfactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionSupport;

import model.User;

public class EditHeaderAction extends ActionSupport {

	public User user;
	private File file;
	private String fileFileName;
	private String fileContentType;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String execute() throws Exception {
		String name = null;
		HttpServletRequest request = ServletActionContext.getRequest();
		user = (User) request.getSession().getAttribute("user");
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		String time = format.format(date);
		String uploadpath = null;
		InputStream is = null;
		OutputStream os = null;
		try {
			// 重命名文件
			name = getFileFileName();// 得到上传文件的原名称
			System.out.println("name"+name);
			int i = name.lastIndexOf(".");// 原名称里倒数第一个"."在哪里
			String ext = name.substring(i + 1);// 取得后缀，及"."后面的字符
			name = user.getUname() + "(" + time + ")" + "." + ext;// 拼凑而成
			setFileFileName(name);
			is = new FileInputStream(file);
			uploadpath = ServletActionContext.getServletContext().getRealPath(
					"/images/header");
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "头像上传失败");
			return "fail";
		}

		File toFile = new File(uploadpath, this.getFileFileName());
		try {
			os = new FileOutputStream(toFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] buffer = new byte[1024];
		int length = 0;

		try {
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}

			is.close();
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("1");
		String picPath = "images/header/" + name;
		System.out.println("name:"+name+"  id"+user.getUid());
		String hql = "update User user set user.upic='" + picPath
				+ "' where user.uid='" + user.getUid()+"'";
		Session session = HibernateSessionFactory.getSession();
		Transaction trans = session.beginTransaction();
		System.out.println("2");
		Query query = session.createQuery(hql);
		System.out.println("s");
		query.executeUpdate();
		System.out.println("3");
		trans.commit();
		request.setAttribute("msg", "头像上传成功");
		System.out.println("4");
		return "success";
	}

}
