package action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Message;
import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import sessionfactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MessageAction2 extends ActionSupport{

	Integer receiveid;
	String turn;
	String hql=null;	
	List <Message> list=null;
	

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession session2=ServletActionContext.getRequest().getSession();
	//Session session=HibernateSessionFactory.getSession();
	
	@SuppressWarnings("unchecked")
	public String execute(){		
		    User user=(User) session2.getAttribute("user"); 	
		    receiveid=user.getUid();	   
		    hql="select m from Message m where m.uidrece='"+receiveid+"'";
		      
		    Configuration conf = new Configuration().configure();
	    	SessionFactory sf = conf.buildSessionFactory();
	    	Session sess = sf.openSession();
	    	Transaction tx = sess.beginTransaction();

		    System.out.println(hql);
		    Query query=sess.createQuery(hql);
		    list=query.list();
		    		    		  
		    //session2.put("Message",list);
		    request.getSession().setAttribute("message",list);
		    		 
		    tx.commit();
		    sess.close();			
		    
		    turn="success";		
		    return turn;
	}

}
