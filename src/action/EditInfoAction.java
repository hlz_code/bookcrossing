package action;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;



import org.apache.struts2.ServletActionContext;
import org.hibernate.*;

import sessionfactory.HibernateSessionFactory;
import utils.MD5;

import com.opensymphony.xwork2.ActionSupport;

import model.User;

public class EditInfoAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	private User user1;
	private String uname;
	private String unickname;
	private String upasswd;
	private String uemail;
	private String uphone;
	private String uwechat;
	private String uqq;
	private String uschool;
	public String getUnickname() {
		return unickname;
	}
	public void setUnickname(String unickname) {
		this.unickname = unickname;
	}
	public String getUpasswd() {
		return upasswd;
	}
	public void setUpasswd(String upasswd) {
		this.upasswd = upasswd;
	}
	public String getUemail() {
		return uemail;
	}
	public void setUemail(String uemail) {
		this.uemail = uemail;
	}
	public String getUphone() {
		return uphone;
	}
	public void setUphone(String uphone) {
		this.uphone = uphone;
	}
	public String getUwechat() {
		return uwechat;
	}
	public void setUwechat(String uwechat) {
		this.uwechat = uwechat;
	}
	public String getUqq() {
		return uqq;
	}
	public void setUqq(String uqq) {
		this.uqq = uqq;
	}
	public String getUschool() {
		return uschool;
	}
	public void setUschool(String uschool) {
		this.uschool = uschool;
	}
	
	public String execute(){
		
		try {
			uschool = new String(uschool.getBytes("ISO-8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(uschool);
		MD5 m=new MD5();
		
		Session session = HibernateSessionFactory.getSession();
		Transaction trans=session.beginTransaction();
		StringBuilder hql= new StringBuilder();
		hql.append("update User user set ");
		if(!"".equals(unickname)){
			hql.append("user.unickname='"+unickname+"', ");
		}
		if(!"".equals(upasswd)){
			hql.append("user.upasswd='"+m.GetMD5Code(upasswd)+"', ");
		}
		if(!"".equals(uemail)){
			hql.append("user.uemail='"+uemail+"', ");
		}
		if(!"".equals(uphone)){
			hql.append("user.uphone='"+uphone+"', ");
		}
		if(!"".equals(uqq)){
			hql.append("user.uqq='"+uqq+"', ");
		}
		if(!"".equals(uwechat)){
			hql.append("user.uwechat='"+uwechat+"', ");
		}
		if(!"".equals(uschool)){
			hql.append("user.uschool='"+uschool+"' ");
		}
		hql.append("where user.uname='"+uname+"'");
		System.out.println(hql.toString());
		Query query = session.createQuery(hql.toString());
		query.executeUpdate();  
        session.getTransaction().commit();  
		
		
		return "success";
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
}
