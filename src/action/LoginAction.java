package action;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.*;

import sessionfactory.HibernateSessionFactory;
import utils.MD5;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
@SuppressWarnings("serial")
public class LoginAction extends ActionSupport {
	private User user;
	public  Integer uid;
	
	@SuppressWarnings({ "rawtypes", "static-access" })
	public String execute() throws Exception{
	    HttpServletRequest request = ServletActionContext.getRequest();
		String username=(String) request.getParameter("username");
		String password=(String) request.getParameter("password");
		String result="FAIL";
		if(username==null||username.equals("")||password==null||password.equals("")){
			request.setAttribute("msg", "账号密码不能为空！");
			return "FAIL";
		}
		MD5 md5=new MD5();
		String cryptedPW = md5.GetMD5Code(password);
		String  hql="select u from User u where u.uname='"+username+"' and u.upasswd='"+cryptedPW+"'";
		Session session=null;
		try{
			session=HibernateSessionFactory.getSession();
			Query query=session.createQuery(hql);			
			List<User> list=query.list();
			// 返回的list存在且不为空，说明成功匹配			
			if(list!=null&&!list.isEmpty()){
				result="SUCCESS";
				Iterator it=list.iterator();
				while(it.hasNext()){
					user=(User) it.next();
				}	
				System.out.println(user.getUname());
			
			if(user.getUflag()==0){
					request.getSession().setAttribute("admin", user);	
				}else{
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("userid", user.getUid());
					request.getSession().setAttribute("username", user.getUname());
				}
			}else{
				request.setAttribute("msg", "验证失败，请输入正确的账号密码。");
			}
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;		
	}
	
}
