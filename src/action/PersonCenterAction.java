package action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;

import sessionfactory.HibernateSessionFactory;
import model.User;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PersonCenterAction extends ActionSupport {

	/**
	 * 查询个人信息
	 */
	@Override
	public String execute() throws Exception {
		String result = "success";
		String name = null;
		HttpServletRequest request = ServletActionContext.getRequest();
		name = (String)request.getSession().getAttribute("username");
		String hql = "select u from User u where u.uname='" + name + "'";
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			Query query = session.createQuery(hql);
			List<User> list = query.list();
			ActionContext.getContext().put("list", list);
			// 返回的list存在且不为空，说明成功匹配
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
