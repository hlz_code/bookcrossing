package action;

import com.opensymphony.xwork2.ActionSupport;

import service.OrderService;
import dao.OrderDao;

public class OrderAction extends ActionSupport{
	
	int Bookid=-1;
	int UidOut;	
	String turn;	
	int UidIn;
	OrderService os;

	public int getUidIn() {
		return UidIn;
	}

	public void setUidIn(int uidIn) {
		UidIn = uidIn;
	}

	public int getBookid() {
		return Bookid;
	}

	public void setBookid(int bookid) {
		Bookid = bookid;
	}
	
	public int getUidOut() {
		return this.UidOut;
	}

	public void setUidOut(int uidOut) {
		this.UidOut = uidOut;
	}
	
	
	public String execute(){
							 
		os =new OrderService();
		OrderDao od=new OrderDao();
		UidOut=od.query(Bookid);	
				
		os.os(Bookid,UidIn,UidOut);	
		
		return os.getTurn();
		
	}

}
