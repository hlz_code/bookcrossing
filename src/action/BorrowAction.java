package action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Book;
import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.opensymphony.xwork2.ActionSupport;


public class BorrowAction extends ActionSupport{
	public String first;
	public String second;
	public Integer require;
	public String bookname;
	public String authorname;
	public String publishername;
	public String type;
	private File file;
	private String fileFileName;
	private String fileContentType;

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public Integer getRequire() {
		return require;
	}

	public void setRequire(Integer require) {
		this.require = require;
	}

	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	public String getAuthorname() {
		return authorname;
	}

	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

	public String getPublishername() {
		return publishername;
	}

	public void setPublishername(String publishername) {
		this.publishername = publishername;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	
	
	
	@SuppressWarnings("deprecation")
	public String execute() throws Exception{
		System.out.println(first+second);
		String name="";
		
		Configuration conf = new Configuration().configure();
		SessionFactory sf = conf.buildSessionFactory();
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Book book=new Book();	
	    HttpServletRequest request = ServletActionContext.getRequest();
	   /* Integer a =(Integer) request.getSession().getAttribute("uid"); 
	    require = (Integer) request.getAttribute("require");
	    bookname = (String) request.getAttribute("bookname");
	    authorname=(String) request.getAttribute("authorname");
	    publishername=(String) request.getAttribute("publishername");
	    type=(String) request.getAttribute("type");
	   */
	    
	    String turn="fail";
	    Date date=new Date();
	    DateFormat format=new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	    String time=format.format(date); 
	  
	 	
	    	String uploadpath = null;
			InputStream is = null;
			OutputStream os = null;
		//	User user=null;			
			HttpSession session=ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user"); 
			
			try {
				//重命名文件
			    name=getFileFileName();//得到上传文件的原名称 
				int i=name.lastIndexOf(".");//原名称里倒数第一个"."在哪里 
				String ext=name.substring(i+1);//取得后缀，及"."后面的字符 
				name=user.getUname()+"("+time+")"+"."+ext;//拼凑而成
				setFileFileName(name);
				is = new FileInputStream(file);
				uploadpath = ServletActionContext.getServletContext().getRealPath(
						"/images/book");
			} catch (Exception e) {
				e.printStackTrace();
				turn= "fail";
			}
			System.out.println(uploadpath);
			// 创建文件
			File toFile = new File(uploadpath, this.getFileFileName());
			try {
				os = new FileOutputStream(toFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			byte[] buffer = new byte[1024];
			int length = 0;

			try {
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				is.close();
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    	type=first+"-"+second;
	    	book.setUid(user.getUid());
	    	book.setBname(bookname);
	    	book.setBauthor(authorname);
	    	book.setBpublisher(publishername);
	    	book.setBtime(time);
	    	book.setBtype(type);
	    	book.setBrequire(require);
	    	book.setBstatus(0);
	    	book.setBpic(name);
	    	sess.save(book);
			tx.commit();
			sess.close();
			if(require==0)
			request.setAttribute("msg", "求购成功！");
			else
			request.setAttribute("msg", "发布成功！");
			turn="success";
	    
	    return turn;
	}

}
