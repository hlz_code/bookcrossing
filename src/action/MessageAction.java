package action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Book;
import model.Message;
import model.Order;
import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import sessionfactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MessageAction extends ActionSupport{
	
	Integer MessageId=0;
	String MessageType="";
    String hql2=null;
    String mcontent;
    String turn;
	
	
	public Integer getMessageId() {
		return this.MessageId;
	}


	public void setMessageId(Integer messageId) {
		this.MessageId = messageId;
	}
	
	public String getMessageType() {
		return this.MessageType;
	}


	public void setMessageType(String messageType) {
		this.MessageType = messageType;
	}
	
	public String execute(){
		
	    

	    Date date=new Date();
	    DateFormat format=new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	    
	    System.out.println(MessageId+"  "+MessageType);
	    
	    if(MessageId!=0&&MessageType.equals("agree")){
	    	
	    	Configuration conf = new Configuration().configure();
	    	SessionFactory sf = conf.buildSessionFactory();
	    	Session sess = sf.openSession();
	    	Transaction tx = sess.beginTransaction();
	    	
	    	hql2="select m from Message m  where m.mid='"+MessageId+"'";
	    	System.out.println(hql2);
	    	Query query2=sess.createQuery(hql2);
		    List<Message> list2=query2.list();
		    if(list2.size()==0){
		    	turn="fail";
		    }else{
		    
		    if(list2.get(0).getMstatus()==0){
		    list2.get(0).setMstatus(1);
		    
		    String hql4="select o from Order o where o.otime='"+list2.get(0).getMtime()+"'";
		    Query query4=sess.createQuery(hql4);
		    List<Order> list4=query4.list();
		    //list4.get(0).setOstatusin(ostatusin);
		    list4.get(0).setOstatusout(1);
		    	    
		   
		    String hql3="select u from User u where u.uid='"+list2.get(0).getUidrece()+"'";
		    Query query3=sess.createQuery(hql3);
		    List<User> list3=query3.list();
		    
		    String hql5="select b from Book b where b.bid='"+list4.get(0).getBid()+"'";
		    Query query5=sess.createQuery(hql5);
		    List<Book> list5=query5.list();
		    
		    mcontent=format.format(date)+"时  "+list3.get(0).getUname()+"已同意了您预约的"+list5.get(0).getBname()+",他的联系方式为:"+list3.get(0).getUphone();
		    
		    Message m =new Message();
		    m.setUidrece(list2.get(0).getUidsent());
		    m.setUidsent(list2.get(0).getUidrece());
		    m.setMcontent(mcontent);
		    m.setMtime(format.format(date));
		    m.setMstatus(2);
		    sess.save(m);
		    
		    
		    
		    }
		    if(list2.get(0).getMstatus()==2){
			    list2.get(0).setMstatus(3);			    	    			 		
			    }
		    					
			turn="success";
		    }
		    tx.commit();
			System.out.print(list2.get(0).getMstatus()+"----------");
			sess.close();	
		    
	    }
	    
	    if(MessageId!=0&&MessageType.equals("refuse")){
	    	
	    	Configuration conf = new Configuration().configure();
	    	SessionFactory sf = conf.buildSessionFactory();
	    	Session sess = sf.openSession();
	    	Transaction tx = sess.beginTransaction();
	    	
	    	hql2="select m from Message m where m.mid="+MessageId;
	    	System.out.println(hql2);
	    	Query query2=sess.createQuery(hql2);
		    List<Message> list2=query2.list();
		    if(list2.size()==0){
		    	turn="fail";
		    }else{
		    if(list2.get(0).getMstatus()==0){
		    list2.get(0).setMstatus(1);
		    		    
            mcontent=format.format(date)+"时  "+list2.get(0).getUidrece()+"已拒绝了您的预约";
		    
		    Message m =new Message();
		    m.setUidrece(list2.get(0).getUidsent());
		    m.setUidsent(list2.get(0).getUidrece());
		    m.setMcontent(mcontent);
		    m.setMtime(format.format(date));
		    m.setMstatus(2);
		    sess.save(m);
		    
		    String hql4="select o from Order o where o.otime='"+list2.get(0).getMtime()+"'";
		    Query query4=sess.createQuery(hql4);
		    List<Order> list4=query4.list();
		    list4.get(0).setOstatusin(0);
		    list4.get(0).setOstatusout(0);
		    
		    }
		    turn="success";		
		    }
		    tx.commit();
			sess.close();
									
			
	    }
	    
	    if(MessageId!=0&&MessageType.equals("delete")){
	    	
	    	Configuration conf = new Configuration().configure();
	    	SessionFactory sf = conf.buildSessionFactory();
	    	Session sess = sf.openSession();
	    	Transaction tx = sess.beginTransaction();
	    	
	    	hql2="delete Message as m where m.mid="+MessageId;
	    	System.out.println(hql2);
	    	Query query2=sess.createQuery(hql2);
	    	query2.executeUpdate();
	    	tx.commit();
			sess.close();
			turn="success";		
					    	
	    }
	    	   	   
	    return turn;
		
	}
	
			 	

}
