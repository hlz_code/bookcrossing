package action;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import model.Book;
import model.User;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;

import sessionfactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class SearchAction extends ActionSupport {
	
	public String first;
	public String second;
	String Keyword="";
	int Bookid=-1;
	String type="";
	int Pagesize=6;//假设每页显示十二条数据
	int Page=0;
	public int PageNum=0;
	
	public String getFirst() {
		return this.first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return this.second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public String getKeyword() {
		return Keyword;
	}


	public void setKeyword(String keyword) {
		this.Keyword = keyword;
	}


	public int getBookid() {
		return Bookid;
	}


	public void setBookid(int bookid) {
		Bookid = bookid;
	}
   

	public int getPage() {
		return Page;
	}


	public void setPage(int page) {
		Page = page;
	}
   

	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}

	

	@SuppressWarnings("unchecked")
	public String execute(){
		HttpServletRequest request = ServletActionContext.getRequest();
		String hql=null;				
		String turn;
		
		System.out.println(Keyword+"   "+type+" "+Bookid);
		
		//翻页时触发的数据库查询
		if(Keyword.equals("")&&Bookid==-1&&type.equals("")){
			hql="select b from Book b where b.bstatus="+0;
		}
		//在搜索框搜索时触发的数据库查询               
		if(!Keyword.equals("")&&Bookid==-1&&type.equals("")){
			hql="select b from Book b where (b.bname like '%"+Keyword+"%' or b.bauthor like '%"+Keyword+"%') and b.bstatus="+0;
		}
		//在点击侧边分类导航时触发的数据库查询
		if(Keyword.equals("")&&Bookid==-1&&!type.equals("")){			
			hql="select b from Book b where b.btype='"+type+"' and b.bstatus ="+0;
		}
		//在点击某一本书时触发的数据库查询
		if(Keyword.equals("")&&Bookid!=-1&&type.equals("")){
			hql="from Book where bid="+Bookid;
		}
		
		try{
		Session session=HibernateSessionFactory.getSession();					
		System.out.println(hql);
		String hql2=hql;
		Query query=session.createQuery(hql);
		Query query2=session.createQuery(hql2);
		
		
		query.setFirstResult((Page-1)*Pagesize);
		query.setMaxResults(Pagesize);
		List<Book> list2=query2.list();
		System.out.println(list2.size());
		
		int count1=list2.size()%Pagesize; 
		int count=list2.size()/Pagesize;  
		
		if(count1>0){		
		PageNum=count+1;
		}else{
			PageNum=count;
		}
		
		String pagenum=String.valueOf(PageNum);
		
		List<Book> list=query.list();	
		
	    System.out.println(list.size());
		
		if(list.size()==0){
			request.setAttribute("msg", "数据库中不存在！");
			turn="fail";
		}else if(Bookid!=-1){
			
			Integer uid=list.get(0).getUid();
			String hql3="select u from User u where u.uid='"+uid+"'";
			Query query3=session.createQuery(hql3);
			List<User> list3=query3.list();	
			User userid=list3.get(0);
			request.setAttribute("userid", userid);
			
			Book book=list.get(0);
			request.setAttribute("Book", book);
			
			turn="info";
			
		}else{
		for(int i=0;i<list.size();i++){
			Book book=list.get(i);
			System.out.println(book.getBname());			
			}
		
		
		ActionContext.getContext().put("Book", list);
				
		request.setAttribute("keyword", Keyword);
		request.setAttribute("type", type);
		request.setAttribute("PageNum",pagenum);										
		request.setAttribute("msg", "查询成功");
		turn="success";
		}
		
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("msg", "查询出错");
			turn="fail";
		}		
	return turn;	
	}

}
