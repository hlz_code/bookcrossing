package model;

/**
 * Book entity. @author MyEclipse Persistence Tools
 */

@SuppressWarnings("serial")
public class Book implements java.io.Serializable {

	// Fields

	private Integer bid;
	private Integer uid;
	private String bname;
	private String bauthor;
	private String bpublisher;
	private String btype;
	private String btime;
	private String bpic;
	private Integer brequire;
	private Integer bstatus;

	// Constructors

	/** default constructor */
	public Book() {
	}

	/** full constructor */
	public Book(Integer uid, String bname, String bauthor, String bpublisher,
			String btype, String btime, String bpic, Integer brequire,
			Integer bstatus) {
		this.uid = uid;
		this.bname = bname;
		this.bauthor = bauthor;
		this.bpublisher = bpublisher;
		this.btype = btype;
		this.btime = btime;
		this.bpic = bpic;
		this.brequire = brequire;
		this.bstatus = bstatus;
	}

	// Property accessors

	public Integer getBid() {
		return this.bid;
	}

	public void setBid(Integer bid) {
		this.bid = bid;
	}

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getBname() {
		return this.bname;
	}

	public void setBname(String bname) {
		this.bname = bname;
	}

	public String getBauthor() {
		return this.bauthor;
	}

	public void setBauthor(String bauthor) {
		this.bauthor = bauthor;
	}

	public String getBpublisher() {
		return this.bpublisher;
	}

	public void setBpublisher(String bpublisher) {
		this.bpublisher = bpublisher;
	}

	public String getBtype() {
		return this.btype;
	}

	public void setBtype(String btype) {
		this.btype = btype;
	}

	public String getBtime() {
		return this.btime;
	}

	public void setBtime(String btime) {
		this.btime = btime;
	}

	public String getBpic() {
		return this.bpic;
	}

	public void setBpic(String bpic) {
		this.bpic = bpic;
	}

	public Integer getBrequire() {
		return this.brequire;
	}

	public void setBrequire(Integer brequire) {
		this.brequire = brequire;
	}

	public Integer getBstatus() {
		return this.bstatus;
	}

	public void setBstatus(Integer bstatus) {
		this.bstatus = bstatus;
	}

}