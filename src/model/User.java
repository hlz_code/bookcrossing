package model;

/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

	// Fields

	private Integer uid;
	private String uname;
	private String unickname;
	private String upasswd;
	private String uemail;
	private String uphone;
	private String uwechat;
	private String uqq;
	private String uschool;
	private String upic;
	private Integer uflag;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(String uname, String upasswd, String uphone, Integer uflag) {
		this.uname = uname;
		this.upasswd = upasswd;
		this.uphone = uphone;
		this.uflag = uflag;
	}

	/** full constructor */
	public User(String uname, String unickname, String upasswd, String uemail,
			String uphone, String uwechat, String uqq, String uschool,
			String upic, Integer uflag) {
		this.uname = uname;
		this.unickname = unickname;
		this.upasswd = upasswd;
		this.uemail = uemail;
		this.uphone = uphone;
		this.uwechat = uwechat;
		this.uqq = uqq;
		this.uschool = uschool;
		this.upic = upic;
		this.uflag = uflag;
	}

	// Property accessors

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUnickname() {
		return this.unickname;
	}

	public void setUnickname(String unickname) {
		this.unickname = unickname;
	}

	public String getUpasswd() {
		return this.upasswd;
	}

	public void setUpasswd(String upasswd) {
		this.upasswd = upasswd;
	}

	public String getUemail() {
		return this.uemail;
	}

	public void setUemail(String uemail) {
		this.uemail = uemail;
	}

	public String getUphone() {
		return this.uphone;
	}

	public void setUphone(String uphone) {
		this.uphone = uphone;
	}

	public String getUwechat() {
		return this.uwechat;
	}

	public void setUwechat(String uwechat) {
		this.uwechat = uwechat;
	}

	public String getUqq() {
		return this.uqq;
	}

	public void setUqq(String uqq) {
		this.uqq = uqq;
	}

	public String getUschool() {
		return this.uschool;
	}

	public void setUschool(String uschool) {
		this.uschool = uschool;
	}

	public String getUpic() {
		return this.upic;
	}

	public void setUpic(String upic) {
		this.upic = upic;
	}

	public Integer getUflag() {
		return this.uflag;
	}

	public void setUflag(Integer uflag) {
		this.uflag = uflag;
	}

}