package model;

public class Order implements java.io.Serializable {

	private Integer oid;
	private Integer uidin;     //借这本书的用户id
	private Integer uidout;    //发布这本书的用户id
	private Integer bid;
	private String otime;
	private Integer ostatusin;   //
	private Integer ostatusout;   //
	
	public Integer getOid() {
		return this.oid;
	}
	public void setOid(Integer oid) {
		this.oid = oid;
	}
	public Integer getUidin() {
		return this.uidin;
	}
	public void setUidin(Integer uidin) {
		this.uidin = uidin;
	}
	public Integer getUidout() {
		return this.uidout;
	}
	public void setUidout(Integer uidout) {
		this.uidout = uidout;
	}
	public Integer getBid() {
		return this.bid;
	}
	public void setBid(Integer bid) {
		this.bid = bid;
	}
	public String getOtime() {
		return this.otime;
	}
	public void setOtime(String otime) {
		this.otime = otime;
	}
	public Integer getOstatusin() {
		return this.ostatusin;
	}
	public void setOstatusin(Integer ostatusin) {
		this.ostatusin = ostatusin;
	}
	public Integer getOstatusout() {
		return this.ostatusout;
	}
	public void setOstatusout(Integer ostatusout) {
		this.ostatusout = ostatusout;
	}
	
}
