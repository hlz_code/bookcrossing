package model;

public class Message implements java.io.Serializable{

	private Integer mid;
	private Integer uidsent;     //发送信息的用户id
	private Integer uidrece;    //接受信息的用户id
	private String mcontent;
	private String mtime;
	private Integer mstatus;   //状态只有两种，0：未读/1：已读
	
	public Integer getMid() {
		return this.mid;
	}
	public void setMid(Integer mid) {
		this.mid = mid;
	}
	public Integer getUidsent() {
		return this.uidsent;
	}
	public void setUidsent(Integer uidsent) {
		this.uidsent = uidsent;
	}
	public Integer getUidrece() {
		return this.uidrece;
	}
	public void setUidrece(Integer uidrece) {
		this.uidrece = uidrece;
	}
	public String getMcontent() {
		return this.mcontent;
	}
	public void setMcontent(String mcontent) {
		this.mcontent = mcontent;
	}
	public String getMtime() {
		return this.mtime;
	}
	public void setMtime(String mtime) {
		this.mtime = mtime;
	}
	public Integer getMstatus() {
		return this.mstatus;
	}
	public void setMstatus(Integer mstatus) {
		this.mstatus = mstatus;
	}
	
	
}
