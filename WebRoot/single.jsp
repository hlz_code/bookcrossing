<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="model.Book" %>
<!DOCTYPE html>
<html>
<head>
<title>图书漂流-图书信息详情</title>
<link rel="shortcut icon"href="images/head.ico"/>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<style>
  .dow{display:block;width:202px;height:52px; line-height:52px; text-align:center; font-family:arial,verdana,sans-serif, '新宋体'; font-weight:bold; font-size:22px; background:#428bca;color:#fff; text-decoration:none; border: 3px solid #357ebd; cursor:pointer}
  .dow:hover{background:#1165ae;}
  .dow:active{background:#0d79d5;}
</style>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.cssmoban.com/statics/css/code-demo.css" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Eshop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->


<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

</head>
<body>
	<!-- header-section-starts -->
	<div class="header">
		<div class="header-top-strip">
			<div class="container">
				
				<div class="header-right">
						<div class="cart box_1">
								
							<p><a href="javascript:;" class="simpleCart_empty">个人中心</a></p>
							<div class="clearfix"> </div>
						</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- header-section-ends -->
	<div class="inner-banner">
		<div class="container">
			<div class="banner-top inner-head">
				<nav class="navbar navbar-default" role="navigation">
	    <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	        </button>
				<div class="logo">
					<h1><a href="index.html">校园图书漂流</a></h1>
				</div>
	    </div>
	    <!--/.navbar-header-->
	
	    
	    <!--/.navbar-collapse-->
	</nav>
	<!--/.navbar-->
</div>
	</div>
		</div>
		<!-- content-section-starts -->
	<div class="container">
	   <div class="products-page">
			<div class="products">
				

			</div>
			<div class="new-product">
				<div class="col-md-5 zoom-grid">
					<div class="flexslider">
						<ul class="slides">
						
							<li data-thumb="images/book/${Book.bpic}">
								<div class="thumb-image"> <img src="images/book/${Book.bpic}" data-imagezoom="true" class="img-responsive" alt="" /> </div>
							</li>
							<li data-thumb="images/book/${Book.bpic}">
								<div class="thumb-image"> <img src="images/book/${Book.bpic}" data-imagezoom="true" class="img-responsive" alt="" /> </div>
							</li>
							<li data-thumb="images/book/${Book.bpic}">
							<div class="thumb-image"> <img src="images/book/${Book.bpic}" data-imagezoom="true" class="img-responsive" alt="" /> </div>
							</li> 
						</ul>
					</div>
				</div>
				<div class="col-md-7 dress-info">
					<div class="dress-name">
						<h3>${Book.bname}</h3>
						
						<div class="clearfix"></div>
						
					</div>
					<div class="span span1">
						<p class="left">作者：</p>
						<p class="right">${Book.bauthor}</p>
						<div class="clearfix"></div>
					</div>
					<div class="span span2">
						<p class="left">发布者：</p>
						<p class="right">${Book.bpublisher}</p>
						<div class="clearfix"></div>
					</div>
					<div class="span span3">
						<p class="left">联系方式：</p>
						<p class="right">发布者接受预定之后方可查看</p>
						<div class="clearfix"></div>
					</div>
					
					<div class="btn_form">
					
					
				         
					<%Integer userid=(Integer)session.getAttribute("userid"); %>
						 
					    <input class="dow" value="预定"  id="${Book.bid}" name="<%=userid%>" onclick="bookClick(this.id,this.name)" >
				        <script type="text/javascript">
  	                  function bookClick(value,name){                
  	                  window.location.href="http://localhost:8080/bookcrossing/OrderAction?Bookid="+value+"&UidIn="+name;
  	                 
  	                  }
  	                  </script>	 
					   
				    </div>
					
					

					<!-- FlexSlider -->
					<script defer src="js/jquery.flexslider.js"></script>
					<script>
						// Can also be used with $(document).ready()
						$(window).load(function() {
						  $('.flexslider').flexslider({
							animation: "slide",
							controlNav: "thumbnails"
						  });
						});
					</script>
				</div>
				<div class="clearfix"></div>
					

			</div>
			<div class="clearfix"></div>
			</div>
   </div>
</body>
</html>