<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<head>
<title>图书漂流-我的个人中心</title>
<link href="css/personinfo/bootstrap.css" rel='stylesheet'
	type='text/css' />

<!-- Custom Theme files -->
<link href="css/personinfo/dashboard.css" rel="stylesheet">
<link href="css/personinfo/style.css" rel='stylesheet' type='text/css' />
<link rel="shortcut icon"href="images/head.ico"/>
<script charset="utf-8"
	src="http://map.qq.com/api/js?v=2.exp?key=YPIBZ-XSC3V-RSSPA-UYPAS-DJ5GE-3KFHU&referer=bookcrossing"></script>
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- start menu -->

</head>
<body>
	<s:iterator value="list" id="u">
		<!-- header -->
		<div class="col-sm-3 col-md-2 sidebar">
			<div class="sidebar_top">
				<h1></h1>
				<a href='http://localhost:8080/bookcrossing/editHeader.jsp'
					target="_blank"><img src="<s:property value="#u.upic" />"
					alt="" /></a>
			</div>
			<div class="details">
				<h3>用户名</h3>
				<p>
					<s:property value="#u.uname" />
				</p>
				<h3>电&nbsp;&nbsp;&nbsp;话</h3>
				<p>
					<s:property value="#u.uphone" />
				</p>
				<h3>邮&nbsp;&nbsp;&nbsp;箱</h3>
				<p>
					<a href="mailto@example.com"><s:property value="#u.uemail" /></a>
				</p>
				<address>
					<h3>学&nbsp;&nbsp;&nbsp;校</h3>
					<p>
						<s:property value="#u.uschool" />
					</p>
				</address>

			</div>
			<div class="clearfix"></div>
		</div>
		<!---->
		<link href="css/personinfo/popuo-box.css" rel="stylesheet"
			type="text/css" media="all" />
		<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<!---//pop-up-box---->
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="content">
				<div class="details_header">
					<ul>
						<li><a target="_blank" href="http://localhost:8080/bookcrossing/PersonCenterAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>信息详情</a></li>
						<li><a target="_blank"
							href="http://localhost:8080/bookcrossing/ModPersonCenterAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>修改信息</a></li>
						<li><a target="_blank"
							href="http://localhost:8080/bookcrossing/PersonPublishAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>我的发布</a></li>
						<li><a target="_blank" href="http://localhost:8080/bookcrossing/PersonOrderAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>我的订单</a></li>
					</ul>
				</div>
				<div style="width:100%;">
					<div class="company">
						<h3 class="clr1">我的发布</h3>
						<br /> <br />
						<div class="form-row-select">
							<div class="content" style="width:100%;margin-left:10%;">
								<h1 class="h1_book_title"></h1>
								<ul>
									<s:iterator value="Book" id="b">
										<div style="float:left;width:30%;">
											<li>
												<dl>
													<dd>
														<img src="images/book/<s:property value="#b.bpic" />"
															alt="book" />
													</dd>
													<dt>
														<p class="book_title">
															书名：
															<s:property value="#b.bname" />
														</p>
														<p class="book_inline">
															作者：
															<s:property value="#b.bauthor" />
														</p>
													</dt>
												</dl>
											</li>
											<br/><br/>
										</div>
									</s:iterator>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<!---->
			</div>
	</s:iterator>
</body>
</html>