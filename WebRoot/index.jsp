<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="Keywords">
<meta name="Description">
<title>图书漂流</title>
<link type="text/css" rel="stylesheet" href="css/reset.css" />
<link type="text/css" rel="stylesheet" href="css/1024_768.css" />
<link type="text/css" rel="stylesheet"
	media="screen and (min-width:861px) and (max-width:960px)"
	href="css/pad_heng.css" />
<link type="text/css" rel="stylesheet"
	media="screen and (min-width:601px) and (max-width:860px)"
	href="css/pad.css" />
<link type="text/css" rel="stylesheet"
	media="screen and (min-width:481px) and (max-width:600px)"
	href="css/tel_heng.css" />
<link type="text/css" rel="stylesheet"
	media="screen and (max-width:480px)" href="css/tel.css" />
<link rel="stylesheet" type="text/css" href="css/publish-style.css" />
<link rel="shortcut icon"href="images/head.ico"/>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.doubleSelect.js"></script>
<link rel="shortcut icon" href="#">
	<link rel="icon" href="#" type="image/gif">
		<link href="./js/style.css" rel="stylesheet" type="text/css">
			<link rel="alternate" type="application/rss+xml" title="#" href="#">
<script type="text/javascript" src="./js/common.js"></script>
<script type="text/javascript" src="./js/index.js"></script>
<style type="text/css">
.float0831 {
	POSITION: fixed;
	TOP: 110px;
	RIGHT: 1px;
	_position: absolute;
	z-index: 999999;
}

.float0831 A {
	COLOR: #00a0e9
}

.float0831 A:hover {
	COLOR: #ff8100;
	TEXT-DECORATION: none
}

.float0831 .floatL {
	POSITION: relative;
	WIDTH: 28px;
	FLOAT: left
}

.float0831 .floatL A {
	TEXT-INDENT: -9999px;
	DISPLAY: block;
	FONT-SIZE: 0px
}

.float0831 .floatR {
	BACKGROUND: url(kf/float_bg.gif)
}

.float0831 .tp {
	BACKGROUND: url(kf/float_bg.gif)
}

.float0831 .cn {
	BACKGROUND: url(kf/float_bg.gif)
}

.float0831 .floatR {
	PADDING-BOTTOM: 15px;
	WIDTH: 130px;
	BACKGROUND-REPEAT: no-repeat;
	BACKGROUND-POSITION: -131px bottom;
	FLOAT: left;
	OVERFLOW: hidden
}

.float0831 .tp {
	BACKGROUND-REPEAT: no-repeat;
	BACKGROUND-POSITION: 0% 0%;
	HEIGHT: 10px
}

.float0831 .cn {
	BACKGROUND-REPEAT: repeat-y;
	BACKGROUND-POSITION: -262px 0px
}

.float0831 .cn H3 {
	TEXT-INDENT: -9999px;
	HEIGHT: 36px;
	FONT-SIZE: 0px
}

.float0831 .cn strong {
	margin-left: 25px;
}

.float0831 .cn UL {
	PADDING-BOTTOM: 0px;
	PADDING-LEFT: 10px;
	PADDING-RIGHT: 10px;
	PADDING-TOP: 0px
}

.float0831 .cn UL LI {
	BORDER-BOTTOM: #e6e5e4 1px solid;
	LINE-HEIGHT: 36px;
	HEIGHT: 36px;
	OVERFLOW: hidden;
	WORD-BREAK: normal
}

.float0831 .titZx {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .titDh {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .titDc, .float0831 .weibo {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .icoZx {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .icoTc {
	BACKGROUND: url(kf/float_s12.gif) no-repeat;
	text-indent: 10px;
}

.float0831 .icoFf {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .icoTl {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .btnOpen {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .btnOpen_n {
	BACKGROUND: url(kf/float_s12.gif) no-repeat
}

.float0831 .btnCtn {
	BACKGROUND: url(kf/float_s12.gif) no-repeat;
	cursor: hand;
}

.float0831 .cn H3.titZx {
	height: 108px;
	BACKGROUND-POSITION: 5px -517px
}

.float0831 .titDh {
	BACKGROUND-POSITION: 5px -66px
}

.float0831 .titDc {
	BACKGROUND-POSITION: 5px -124px
}

.float0831 .weibo {
	margin-top: 7px;
	BACKGROUND-POSITION: 5px -627px
}

.float0831 .icoZx {
	BACKGROUND-POSITION: 2px -154px
}

.float0831 .icoTc {
	BACKGROUND-POSITION: 0px -323px
}

.float0831 .icoFf {
	BACKGROUND-POSITION: 2px -213px
}

.float0831 .icoTl {
	PADDING-LEFT: 20px;
	DISPLAY: block;
	FONT-FAMILY: 微软雅黑;
	BACKGROUND-POSITION: 2px -266px;
	FLOAT: left
}

.float0831 .btnOpen {
	BACKGROUND-POSITION: -30px -396px
}

.float0831 .btnOpen_n {
	BACKGROUND-POSITION: 0px -530px
}

.float0831 .btnCtn {
	BACKGROUND-POSITION: 0px -396px
}

.float0831 .icoZx {
	PADDING-LEFT: 28px;
	DISPLAY: block;
	FLOAT: left
}

.float0831 .icoTc {
	PADDING-LEFT: 28px;
	DISPLAY: block;
	FLOAT: left
}

.float0831 .icoFf {
	PADDING-LEFT: 28px;
	DISPLAY: block;
	FLOAT: left
}

.float0831 .btnOpen {
	POSITION: relative;
	WIDTH: 28px;
	HEIGHT: 118px;
	TOP: 80px;
	LEFT: 2px
}

.float0831 .btnOpen_n {
	POSITION: relative;
	WIDTH: 28px;
	HEIGHT: 118px;
	TOP: 80px;
	LEFT: 2px
}

.float0831 .btnCtn {
	POSITION: relative;
	WIDTH: 28px;
	HEIGHT: 118px;
	TOP: 80px;
	LEFT: 2px;
	cursor: hand;
}

.float0831 .btnOpen {
	TOP: 118px;
	LEFT: 1px
}

.float0831 .btnOpen_n {
	TOP: 118px;
	LEFT: 1px
}

.float0831 .btnCtn {
	TOP: 118px;
	LEFT: 2px
}

.float0831 UL LI H3.titDc A, .float0831 UL LI H3.weibo A {
	WIDTH: 80px;
	DISPLAY: block;
	HEIGHT: 36px
}

.float0831 UL LI.top {
	height: 108px;
}

.float0831 UL LI.bot {
	BORDER-BOTTOM-STYLE: none
}

.float0831 UL.webZx {
	PADDING-BOTTOM: 0px;
	PADDING-LEFT: 0px;
	WIDTH: 164px;
	PADDING-RIGHT: 0px;
	BACKGROUND: url(kf/webzx_bg1.jpg) no-repeat;
	HEIGHT: 14px;
	PADDING-TOP: 0px
}

* html .float0831 {
	position: absolute;
	left: expression(eval(document.documentElement.scrollRight +0));
	top: expression(eval(document.documentElement.scrollTop +100))
}

#Fchat1 {
	display: none;
}

#Fchat2 {
	width: 29px;
	height: 117px;
	position: fixed;
	right: 0px;
	top: 210px;
	cursor: pointer;
	display: block;
	BACKGROUND: url(kf/online.gif) no-repeat;
	_position: absolute;
	_top: expression(eval(document.documentElement.scrollTop +190));
}

.bdshare-button-style0-16 a, .bdshare-button-style0-16 .bds_more {
	margin: 6px 4px 6px 0 !important;
}
</style>
<script src="./js/share.js"></script>
<link rel="stylesheet" href="./js/share_style0_16.css">
</head>
<body class="root61" ryt13623="1">
	<script type="text/javascript" src="./js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="./js/jquery.lazyload.min.js"></script>
	<script type="text/javascript" src="./js/jquery.json.js"></script>
	<script type="text/javascript" src="./js/jd_common.js"></script>
	<script type="text/javascript" src="./js/easydialog.min.js"></script>
	<script type="text/javascript" src="./js/transport_jquery.js"></script>
	<script type="text/javascript" src="./js/utils.js"></script>
	<script type="text/javascript" src="./js/jquery.SuperSlide.js"></script>
	<script>
    $(function () {
        $("img[data-original]").lazyload({threshold: 200});
     });
</script>
	<div id="shortcut-2013">
		<div class="w">
			<ul class="fr lh">

				<%String username=(String)session.getAttribute("username");
                if(username==null){%>
				<li class="fore1" id="loginbar">
					<div class="dt">
						<span><a
							href="http://localhost:8080/bookcrossing/login.jsp">你好，请登录
								&nbsp;</a> <a href="http://localhost:8080/bookcrossing/register.jsp"
							class="link-regist" style="color:#C81623;">免费注册</a> </span>
					</div>
				</li>


				<li class="fore2 ld"><div class="dt">
						<s></s><a href="#"">个人中心</a>
					</div></li>
				<%}else{ %>
				<li class="fore1" id="loginbar">
					<div class="dt">
						<span>你好,<%=username%> &nbsp;<a href="#"
							class="link-regist" style="color:#C81623;">欢迎回来!</a> &nbsp;<a
							href="http://localhost:8080/bookcrossing/MessageAction2"
							target="_blank" class="link-regist" style="color:#C81623;"><img
								src="./images/mail.png" />消息</a></span>
					</div>
				</li>


				<li class="fore2 ld"><div class="dt">
						<s></s><a target="_blank"
							href="http://localhost:8080/bookcrossing/PersonCenterAction"
							onclick="dClick()">个人中心</a>
					</div></li>
				<%} %>



				<script type="text/javascript">
			if (document.getElementById('history_list').innerHTML.replace(/\s/g,'').length<1){
				document.getElementById('history_div').style.display='none';
			}else{
				document.getElementById('history_div').style.display='block';
			}
			function clear_history(){
				Ajax.call('user.php', 'act=clear_history',clear_history_Response, 'GET', 'TEXT',1,1);
			}
			function clear_history_Response(res){
				document.getElementById('history_list').innerHTML = '<center>您已清空最近浏览过的商品</center>';
			}
			$(function(){
				var zhi=$(".viewList").find(".smc .item").length;
				if( zhi > 4 ){
					$(".viewList").find(".smc .item").eq(3).nextAll().remove();
				}
			});
            </script>


			</ul>
		</div>
	</div>
	<div id="o-header-2013">
		<div class="w" id="header-2013">
			<div id="logo-2013" class="ld">
				<a href="#	" hidefocus="true"><img src="./images/logo1.jpg"
					alt="文迪"></a>
			</div>

			<div id="search-2013">
				<div class="i-search ld">
					<div class="form">

						<input type="text" class="text" accesskey="s" id="key"
							autocomplete="off" name="Keyword" value=""> <input
							type="button" value="搜索" class="button" onclick="searchClick2()">

						<script type="text/javascript">
  	                  function searchClick2(){
  	                  var u = document.getElementById("key").value;
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?Keyword="+u;
  	                  }
  	                  </script>
					</div>
				</div>
			</div>

			<div id="my360buy-2013" class="hide">
				<dl>
					<dt class="ld">
						<s></s><a href="http://www.jieshuwang.com/user.php">我的</a><b></b>
					</dt>
					<dd>
						<div class="prompt">
							<span class="fl"><strong></strong></span> <span class="fr"><a
								href="http://www.jieshuwang.com/user.php">去我的文迪首页&nbsp;&gt;</a></span>
						</div>
						<div id="jduc-orderlist">
							<div class="orderlist">
								<div class="smt">
									<h4>最新订单状态：</h4>
									<div class="extra">
										<a href="http://www.jieshuwang.com/user.php?act=order_list"
											target="_blank">查看所有订单&nbsp;&gt;</a>
									</div>
								</div>
								<div class="smc">
									<ul></ul>
								</div>
							</div>
						</div>
						<div class="uclist">
							<ul class="fore1 fl">
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=address_list">收货地址&nbsp;&gt;</a></li>
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=booking_list">缺货登记&nbsp;&gt;</a></li>
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=profile">用户信息&nbsp;&gt;</a></li>
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=collection_list">我的收藏&nbsp;&gt;</a></li>
							</ul>
							<ul class="fore2 fl">
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=message_list">我的留言&nbsp;&gt;</a></li>
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=comment_list">我的评论&nbsp;&gt;</a></li>
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=bonus">我的红包&nbsp;&gt;</a></li>
								<li><a target="_blank"
									href="http://www.jieshuwang.com/user.php?act=bonus">资金管理&nbsp;&gt;</a></li>
							</ul>
						</div>
						<div class="viewlist" id="history_div">
							<div class="smt">
								<h4>最近浏览的商品：</h4>
							</div>
							<div class="smc" id="jduc-viewlist">
								<ul class="lh hide" style="display: block;" id="history_list">
								</ul>
							</div>
						</div>
						<script type="text/javascript">
                        if (document.getElementById('history_list').innerHTML.replace(/\s/g,'').length<1){
                            document.getElementById('history_div').style.display='none';
                        }else{
                            document.getElementById('history_div').style.display='block';
                        };
                        function clear_history(){
                            Ajax.call('user.php', 'act=clear_history',clear_history_Response, 'GET', 'TEXT',1,1);
                        };
                        function clear_history_Response(res){
                            document.getElementById('history_list').innerHTML = '<center>您已清空最近浏览过的商品</center>';
                        };
                    </script>
					</dd>
				</dl>
			</div>



		</div>

		<div class="navBox">
			<div class="w">
				<div id="nav-2013">
					<div id="categorys-2013" class="categorys-2014 ">
						<div class="mt ld">
							<h2>
								<a href="javascript:;">全部图书分类<b></b></a>
							</h2>
						</div>
						<div id="_JD_ALLSORT" class="mc">
							<div class="item fore1"></div>
							<div class="item fore2">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437884990383816331.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">计算机与软件类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="1-1" onclick="searchClick(this.id)">Android</a><a
													href="#" id="1-2" onclick="searchClick(this.id)">Java</a><a
													href="#" id="1-3" onclick="searchClick(this.id)">iOS</a><a
													href="#" id="1-4" onclick="searchClick(this.id)">JavaScript</a><a
													href="#" id="1-5" onclick="searchClick(this.id)">PHP</a><a
													href="#" id="1-6" onclick="searchClick(this.id)">C++</a><a
													href="#" id="1-7" onclick="searchClick(this.id)">C#</a><a
													href="#" id="1-8" onclick="searchClick(this.id)">HTML5</a><a
													href="#" id="1-9" onclick="searchClick(this.id)">JSP</a><a
													href="#" id="1-10" onclick="searchClick(this.id)">C语言</a>
											</dd>
										</dl>
										<script type="text/javascript">
  	                                    function searchClick(id){ 	                             
  	                                    window.location.href="http://localhost:8080/bookcrossing/SearchAction?type="+id;
  	                                    }
  	                                    </script>
										<dl class="fore2">

											<dd>
												<a href="#" id="1-11" onclick="searchClick(this.id)">Excel</a><a
													href="#" id="1-12" onclick="searchClick(this.id)">PPT</a><a
													href="#" id="1-13" onclick="searchClick(this.id)">Word</a><a
													href="#" id="1-14" onclick="searchClick(this.id)">Office合集</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="1-15" onclick="searchClick(this.id)">DreamWeaver</a><a
													href="#" id="1-16" onclick="searchClick(this.id)">Flash</a><a
													href="#" id="1-17" onclick="searchClick(this.id)">Fireworks</a><a
													href="#" id="1-18" onclick="searchClick(this.id)">Authorware</a><a
													href="#" id="1-19" onclick="searchClick(this.id)">CSS</a><a
													href="#" id="1-20" onclick="searchClick(this.id)">FrontPage</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="1-21" onclick="searchClick(this.id)">网络组件</a><a
													href="#" id="1-22" onclick="searchClick(this.id)">网络协议</a><a
													href="#" id="1-23" onclick="searchClick(this.id)">网络管理</a><a
													href="#" id="1-24" onclick="searchClick(this.id)">网络理论</a><a
													href="#" id="1-25" onclick="searchClick(this.id)">WebServer</a>
											</dd>
										</dl>
										<dl class="fore5">

											<dd>
												<a href="#" id="1-26" onclick="searchClick(this.id)">SQL语言</a><a
													href="#" id="1-27" onclick="searchClick(this.id)">Oracle</a><a
													href="#" id="1-28" onclick="searchClick(this.id)">MySQL</a>
											</dd>
										</dl>
										<dl class="fore6">

											<dd>
												<a href="#" id="1-29" onclick="searchClick(this.id)">Linux</a><a
													href="#" id="1-30" onclick="searchClick(this.id)">Unix</a><a
													href="#" id="1-31" onclick="searchClick(this.id)">Mac
													OS</a><a href="#" id="1-32" onclick="searchClick(this.id)">Windows</a><a
													href="#" id="1-33" onclick="searchClick(this.id)">DOS</a><a
													href="#" id="1-34" onclick="searchClick(this.id)">系统开发</a>
											</dd>
										</dl>

										<dl class="fore8">

											<dd>
												<a href="#" id="1-35" onclick="searchClick(this.id)">PhotoShop</a><a
													href="#" id="1-36" onclick="searchClick(this.id)">3ds
													MAX</a><a href="#" id="1-37" onclick="searchClick(this.id)">Premiere</a><a
													href="#" id="1-38" onclick="searchClick(this.id)">MAYA</a><a
													href="#" id="1-39" onclick="searchClick(this.id)">Painter</a><a
													href="#" id="1-40" onclick="searchClick(this.id)">Freehand</a>
											</dd>
										</dl>
										<dl class="fore9">

											<dd>
												<a href="#" id="1-41" onclick="searchClick(this.id)">机器学习</a><a
													href="#" id="1-42" onclick="searchClick(this.id)">软件工程</a><a
													href="#" id="1-43" onclick="searchClick(this.id)">计算机安全</a><a
													href="#" id="1-44" onclick="searchClick(this.id)">电子商务</a><a
													href="#" id="1-45" onclick="searchClick(this.id)">移动开发</a><a
													href="#" id="1-46" onclick="searchClick(this.id)">大数据与云计算</a>
											</dd>
										</dl>


									</div>
									<div class="cat-right-con fr" id="JD_sort_a">
										<div class="itemBrands"></div>

									</div>
								</div>
							</div>
							<div class="item fore3">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437885154415839099.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">经济管理类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="2-1" onclick="searchClick(this.id)">市场营销</a><a
													href="#" id="2-2" onclick="searchClick(this.id)">企业管理</a><a
													href="#" id="2-3" onclick="searchClick(this.id)">团队建设</a><a
													href="#" id="2-4" onclick="searchClick(this.id)">财务管理</a><a
													href="#" id="2-5" onclick="searchClick(this.id)">人力资源</a><a
													href="#" id="2-6" onclick="searchClick(this.id)">供应链</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="2-7" onclick="searchClick(this.id)">经济史</a><a
													href="#" id="2-8" onclick="searchClick(this.id)">会计/审计</a><a
													href="#" id="2-9" onclick="searchClick(this.id)">国际贸易</a><a
													href="#" id="2-10" onclick="searchClick(this.id)">国际金融</a><a
													href="#" id="2-11" onclick="searchClick(this.id)">互联网金融</a><a
													href="#" id="2-12" onclick="searchClick(this.id)">中国经济</a><a
													href="#" id="2-13" onclick="searchClick(this.id)">世界经济</a><a
													href="#" id="2-14" onclick="searchClick(this.id)">财政税收</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="2-15" onclick="searchClick(this.id)">股票</a><a
													href="#" id="2-16" onclick="searchClick(this.id)">期权</a><a
													href="#" id="2-17" onclick="searchClick(this.id)">期货</a><a
													href="#" id="2-18" onclick="searchClick(this.id)">基金</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="2-19" onclick="searchClick(this.id)">大数据</a><a
													href="#" id="2-20" onclick="searchClick(this.id)">网络营销</a><a
													href="#" id="2-21" onclick="searchClick(this.id)">客户服务</a><a
													href="#" id="2-22" onclick="searchClick(this.id)">搜索优化</a><a
													href="#" id="2-23" onclick="searchClick(this.id)">电商思维</a><a
													href="#" id="2-24" onclick="searchClick(this.id)">互联网+</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore4">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437885738615042734.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">电信类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="3-1" onclick="searchClick(this.id)">模拟电子技术</a><a
													href="#" id="3-2" onclick="searchClick(this.id)">数字电子技术</a><a
													href="#" id="3-3" onclick="searchClick(this.id)">微机原理与接口</a><a
													href="#" id="3-4" onclick="searchClick(this.id)">信息论基础</a><a
													href="#" id="3-5" onclick="searchClick(this.id)">计算机网络与通信</a><a
													href="#" id="3-6" onclick="searchClick(this.id)">嵌入式系统</a><a
													href="#" id="3-7" onclick="searchClick(this.id)">EDA技术</a><a
													href="#" id="3-8" onclick="searchClick(this.id)">信号与系统</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="3-9" onclick="searchClick(this.id)">模拟电子线路</a><a
													href="#" id="3-10" onclick="searchClick(this.id)">数字电子线路</a><a
													href="#" id="3-11" onclick="searchClick(this.id)">精密机械与仪器设计</a><a
													href="#" id="3-12" onclick="searchClick(this.id)">精密机械制造工程</a><a
													href="#" id="3-13" onclick="searchClick(this.id)">微型计算机原理与应用</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="3-14" onclick="searchClick(this.id)">热工学</a><a
													href="#" id="3-15" onclick="searchClick(this.id)">工程材料与热处理</a><a
													href="#" id="3-16" onclick="searchClick(this.id)">电工与电子技术</a><a
													href="#" id="3-17" onclick="searchClick(this.id)">电力服务系统</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="3-18" onclick="searchClick(this.id)">TCP/IP协议</a><a
													href="#" id="3-19" onclick="searchClick(this.id)">物联网技术</a><a
													href="#" id="3-20" onclick="searchClick(this.id)">数字信号处理</a><a
													href="#" id="3-21" onclick="searchClick(this.id)">信号与系统</a><a
													href="#" id="3-22" onclick="searchClick(this.id)">RFID原理与应用</a><a
													href="#" id="3-23" onclick="searchClick(this.id)">通信电子线路</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore5">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437884811004688156.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">交通运输类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="4-1" onclick="searchClick(this.id)">工程图学</a><a
													href="#" id="4-2" onclick="searchClick(this.id)">汽车构造</a><a
													href="#" id="4-3" onclick="searchClick(this.id)">机械设计</a><a
													href="#" id="4-4" onclick="searchClick(this.id)">交通运输设备</a><a
													href="#" id="4-5" onclick="searchClick(this.id)">交通运输法规</a><a
													href="#" id="4-6" onclick="searchClick(this.id)">交通规划</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="4-7" onclick="searchClick(this.id)">理论力学</a><a
													href="#" id="4-8" onclick="searchClick(this.id)">汽车设计</a><a
													href="#" id="4-9" onclick="searchClick(this.id)">汽车制造工艺学</a><a
													href="#" id="4-10" onclick="searchClick(this.id)">运输市场营销</a><a
													href="#" id="4-11" onclick="searchClick(this.id)">交通港站与枢纽</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="4-12" onclick="searchClick(this.id)">材料力学</a><a
													href="#" id="4-13" onclick="searchClick(this.id)">汽车理论</a><a
													href="#" id="4-14" onclick="searchClick(this.id)">机械原理</a><a
													href="#" id="4-15" onclick="searchClick(this.id)">城市轨道交通客运管理</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="4-16" onclick="searchClick(this.id)">结构力学</a><a
													href="#" id="4-17" onclick="searchClick(this.id)">热工与发动机原理</a><a
													href="#" id="4-18" onclick="searchClick(this.id)">道路建筑材料</a><a
													href="#" id="4-19" onclick="searchClick(this.id)">轨道交通信号原理</a><a
													href="#" id="4-20" onclick="searchClick(this.id)">交通运输组织学</a>
											</dd>
										</dl>
										<dl class="fore5">

											<dd>
												<a href="#" id="4-21" onclick="searchClick(this.id)">测量学</a><a
													href="#" id="4-22" onclick="searchClick(this.id)">液压与气压传动</a><a
													href="#" id="4-23" onclick="searchClick(this.id)">智能交通</a><a
													href="#" id="4-24" onclick="searchClick(this.id)">交通控制与管理</a><a
													href="#" id="4-25" onclick="searchClick(this.id)">现代物流学</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore6">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437885843277612724.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">土木工程类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="5-1" onclick="searchClick(this.id)">基础工程(道桥)</a><a
													href="#" id="5-2" onclick="searchClick(this.id)">线路工程</a><a
													href="#" id="5-3" onclick="searchClick(this.id)">工程项目管理</a><a
													href="#" id="5-4" onclick="searchClick(this.id)">结构设计原理(钢结构)</a><a
													href="#" id="5-5" onclick="searchClick(this.id)">结构抗震及高层建筑</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="5-6" onclick="searchClick(this.id)">土木工程项目管理</a><a
													href="#" id="5-7" onclick="searchClick(this.id)">钢桥</a><a
													href="#" id="5-8" onclick="searchClick(this.id)">工程合同管理</a><a
													href="#" id="5-9" onclick="searchClick(this.id)">桥渡设计</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="5-10" onclick="searchClick(this.id)">工程流体力学</a><a
													href="#" id="5-11" onclick="searchClick(this.id)">桥隧工程</a><a
													href="#" id="5-12" onclick="searchClick(this.id)">投资风险管理学</a><a
													href="#" id="5-13" onclick="searchClick(this.id)">工程数学</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="5-14" onclick="searchClick(this.id)">道路勘测设计</a><a
													href="#" id="5-15" onclick="searchClick(this.id)">工程监理</a><a
													href="#" id="5-16" onclick="searchClick(this.id)">建设与房地产法规</a><a
													href="#" id="5-17" onclick="searchClick(this.id)">工程估价</a>
											</dd>
										</dl>
										<dl class="fore5">

											<dd>
												<a href="#" id="5-18" onclick="searchClick(this.id)">路基路面工程</a><a
													href="#" id="5-19" onclick="searchClick(this.id)">建筑设备</a><a
													href="#" id="5-20" onclick="searchClick(this.id)">工程承包与招投标</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore7">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437885891284533895.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">电气工程类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="6-1" onclick="searchClick(this.id)">电路原理</a><a
													href="#" id="6-2" onclick="searchClick(this.id)">电机学</a><a
													href="#" id="6-3" onclick="searchClick(this.id)">信号与系统</a><a
													href="#" id="6-4" onclick="searchClick(this.id)">电力系统分析</a><a
													href="#" id="6-5" onclick="searchClick(this.id)">检测与转换技术</a><a
													href="#" id="6-6" onclick="searchClick(this.id)">工程制图</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="6-7" onclick="searchClick(this.id)">电子技术基础</a><a
													href="#" id="6-8" onclick="searchClick(this.id)">电力电子技术</a><a
													href="#" id="6-9" onclick="searchClick(this.id)">自动控制理论</a><a
													href="#" id="6-10" onclick="searchClick(this.id)">电气控制技术</a><a
													href="#" id="6-11" onclick="searchClick(this.id)">电力系统继电保护</a><a
													href="#" id="6-12" onclick="searchClick(this.id)">电机学</a>
											</dd>
										</dl>


									</div>

								</div>
							</div>
							<div class="item fore8">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437896896166683282.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">机电类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="7-1" onclick="searchClick(this.id)">材料物理化学</a><a
													href="#" id="7-2" onclick="searchClick(this.id)">液压传动</a><a
													href="#" id="7-3" onclick="searchClick(this.id)">机械设计学</a><a
													href="#" id="7-4" onclick="searchClick(this.id)">机械系统设计</a><a
													href="#" id="7-5" onclick="searchClick(this.id)">机械动力学</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="7-6" onclick="searchClick(this.id)">汽车构造</a><a
													href="#" id="7-7" onclick="searchClick(this.id)">发动机原理</a><a
													href="#" id="7-8" onclick="searchClick(this.id)">汽车理论</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="7-9" onclick="searchClick(this.id)">电力电子技术</a><a
													href="#" id="7-10" onclick="searchClick(this.id)">智能仪器</a><a
													href="#" id="7-11" onclick="searchClick(this.id)">虚拟仪器</a><a
													href="#" id="7-12" onclick="searchClick(this.id)">工程力学</a><a
													href="#" id="7-13" onclick="searchClick(this.id)">传感器技术</a><a
													href="#" id="7-14" onclick="searchClick(this.id)">物联网技术与应用</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="7-15" onclick="searchClick(this.id)">工业工程基础</a><a
													href="#" id="7-16" onclick="searchClick(this.id)">企业信息化</a><a
													href="#" id="7-17" onclick="searchClick(this.id)">生产管理与控制</a><a
													href="#" id="7-18" onclick="searchClick(this.id)">人机工程学</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore9">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437897006452192904.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">理学类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="8-1" onclick="searchClick(this.id)">数学分析</a><a
													href="#" id="8-2" onclick="searchClick(this.id)">数学模型</a><a
													href="#" id="8-3" onclick="searchClick(this.id)">金融数学</a><a
													href="#" id="8-4" onclick="searchClick(this.id)">数据分析系统教程</a><a
													href="#" id="8-5" onclick="searchClick(this.id)">金融计量经济学</a><a
													href="#" id="8-6" onclick="searchClick(this.id)">常微分方程</a><a
													href="#" id="8-7" onclick="searchClick(this.id)">微观经济学</a><a
													href="#" id="8-8" onclick="searchClick(this.id)">宏观经济学</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="8-9" onclick="searchClick(this.id)">信息论与编码</a><a
													href="#" id="8-10" onclick="searchClick(this.id)">离散数学</a><a
													href="#" id="8-11" onclick="searchClick(this.id)">图形图像处理技术</a><a
													href="#" id="8-12" onclick="searchClick(this.id)">计算机图形学</a><a
													href="#" id="8-13" onclick="searchClick(this.id)">数值分析</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore10">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437897630629709368.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">语言与传播类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="9-1" onclick="searchClick(this.id)">综合英语</a><a
													href="#" id="9-2" onclick="searchClick(this.id)">高级英语</a><a
													href="#" id="9-3" onclick="searchClick(this.id)">英语口语</a><a
													href="#" id="9-4" onclick="searchClick(this.id)">写作</a><a
													href="#" id="9-5" onclick="searchClick(this.id)">听力</a>
											</dd>
										</dl>

										<dl class="fore3">

											<dd>
												<a href="#" id="9-6" onclick="searchClick(this.id)">新闻学概论</a><a
													href="#" id="9-7" onclick="searchClick(this.id)">新闻采访与写作</a><a
													href="#" id="9-8" onclick="searchClick(this.id)">大众传播学</a><a
													href="#" id="9-9" onclick="searchClick(this.id)">广告学</a>
											</dd>
										</dl>


									</div>

								</div>
							</div>
							<div class="item fore11">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437897982618233982.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">建筑与艺术类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="10-1" onclick="searchClick(this.id)">建筑力学</a><a
													href="#" id="10-2" onclick="searchClick(this.id)">建筑材料</a><a
													href="#" id="10-3" onclick="searchClick(this.id)">建筑构造</a><a
													href="#" id="10-4" onclick="searchClick(this.id)">中外建筑史</a><a
													href="#" id="10-5" onclick="searchClick(this.id)">公共建筑设计原理</a><a
													href="#" id="10-6" onclick="searchClick(this.id)">室内设计</a><a
													href="#" id="10-7" onclick="searchClick(this.id)">城市设计概论</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="10-8" onclick="searchClick(this.id)">中外美术史</a><a
													href="#" id="10-9" onclick="searchClick(this.id)">设计基础</a>
											</dd>
										</dl>
										<dl class="fore3">

											<dd>
												<a href="#" id="10-10" onclick="searchClick(this.id)">艺术设计史</a><a
													href="#" id="10-11" onclick="searchClick(this.id)">设计素描</a><a
													href="#" id="10-12" onclick="searchClick(this.id)">版式设计</a>
											</dd>
										</dl>
										<dl class="fore4">

											<dd>
												<a href="#" id="10-13" onclick="searchClick(this.id)">植物学</a><a
													href="#" id="10-14" onclick="searchClick(this.id)">景观生态学</a><a
													href="#" id="10-15" onclick="searchClick(this.id)">中国古典园林史</a>
											</dd>
										</dl>
										<dl class="fore5">

											<dd>
												<a href="#" id="10-16" onclick="searchClick(this.id)">城乡规划原理</a><a
													href="#" id="10-17" onclick="searchClick(this.id)">城乡规划设计</a><a
													href="#" id="10-18" onclick="searchClick(this.id)">城市生态环境保护</a><a
													href="#" id="10-19" onclick="searchClick(this.id)">区域规划</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore12">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437898374217802155.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">法学类</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="11-1" onclick="searchClick(this.id)">理论法学</a><a
													href="#" id="11-2" onclick="searchClick(this.id)">法理学</a><a
													href="#" id="11-3" onclick="searchClick(this.id)">法哲学</a><a
													href="#" id="11-4" onclick="searchClick(this.id)">法律社会学</a><a
													href="#" id="11-5" onclick="searchClick(this.id)">法律逻辑学</a><a
													href="#" id="11-6" onclick="searchClick(this.id)">立法学</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="11-7" onclick="searchClick(this.id)">社会研究方法</a><a
													href="#" id="11-8" onclick="searchClick(this.id)">公共关系学</a><a
													href="#" id="11-9" onclick="searchClick(this.id)">管理心理学</a>
											</dd>
										</dl>

									</div>

								</div>
							</div>
							<div class="item fore13">
								<span><h3
										style="background:url(http://jieshuwang.com/data/category/1437899517235496488.png) 5% center no-repeat; padding-left:35px;">
										<a href="#">基础课程</a>
									</h3> <s></s></span>
								<div class="i-mc" style="height: 479px;">
									<div class="subitem">
										<dl class="fore1">

											<dd>
												<a href="#" id="12-1" onclick="searchClick(this.id)">线性代数</a><a
													href="#" id="12-2" onclick="searchClick(this.id)">微积分</a>
											</dd>
										</dl>
										<dl class="fore2">

											<dd>
												<a href="#" id="12-3" onclick="searchClick(this.id)">毛泽东思想概论</a><a
													href="#" id="12-4" onclick="searchClick(this.id)">马克思主义哲学</a>
											</dd>
										</dl>


									</div>

								</div>
							</div>


						</div>
						<script>
                    $(function(){
						$("#_JD_ALLSORT .item").each(function(){
							var h=$(this).find(".i-mc").outerHeight();
							if( h<466){
								$(this).find(".i-mc").height(479);
							};
						});
					});
                    </script>
					</div>
					<ul id="navitems-2013">
						<li class="fore1 curr"><a href="#">首页</a></li>
						<li class="fore1 "><a href="#">介绍</a></li>
						<li class="fore2 "><a
							href="http://localhost:8080/bookcrossing/publish.jsp"
							target="_blank">发布书籍</a></li>
						<li class="fore3 "><a
							href="http://localhost:8080/bookcrossing/publish.jsp"
							target="_blank">发布求购</a></li>
						<li class="fore4 "><a href="#">活动</a></li>
						<li class="fore5 "><a href="#">书友</a></li>
						<li class="fore6 "><a href="#" target="_blank">小黑屋</a></li>

					</ul>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="./js/jd_index.js"></script>
	<style type="text/css">
/*首页播放器开始*/
.slideBox {
	width: 100%;
	height: 480px;
	overflow: hidden;
}

.slideBox .hd {
	width: 260px;
	left: 48%;
	margin-top: 440px;
	position: absolute;
	z-index: 90;
	display: block
}

.slideBox .hd ul {
	overflow: hidden;
	zoom: 1;
	float: left;
	height: 25px;
}

.slideBox .hd ul li {
	display: inline-block;
	width: 20px;
	float: left;
	height: 20px;
	background: black;
	-webkit-border-radius: 50%;
	-moz-border-radius: 50%;
	border-radius: 50%;
	margin-right: 5px;
	cursor: pointer;
	color: white;
	font-family: Arial;
	text-align: center;
	line-height: 20px;
}

.slideBox .hd ul li.on {
	background: #D8A504;
}

.banner_main_con {
	width: 1190px;
	margin: 0 auto;
	height: 480px;
	position: relative;
}

.main_banner_a { /*margin-left: 240px;*/
	display: block;
	width: 1190px;
	height: 480px;
	background: url(about:blank);
}
/*.mall_banner_ad i {position: absolute; cursor: pointer; top: 0; left: 0; background: #000; _background: none; border: none; height: 121px; width: 192px;opacity: 0;
filter: alpha(opacity=0);}*/
.slideBox .bd {
	position: relative;
	width: 100%;
	height: 480px;
	overflow: hidden
}

.slideBox .bd li {
	vertical-align: middle;
	height: 480px;
}

.slideBox .bd img {
	display: block;
}

.mall_banner_ad .mall_ban {
	display: block;
	position: relative
}

.mall_banner_ad a img {
	vertical-align: middle;
	position: absolute;
	left: 5px;
	top: 0;
}
</style>
	<div id="slideBox" class="slideBox">
		<div class="hd ">
			<ul>

				<li class="">1</li>


				<li class="">2</li>


				<li class="">3</li>


				<li class="">4</li>



			</ul>
		</div>
		<div class="bd">
			<ul>

				<li class="li_1"
					style="display: none; background: url(./images/1.jpg) 50% 0% no-repeat;">
					<div class="banner_main_con">
						<a href="#" target="_blank" class="main_banner_a"></a>
					</div>
				</li>


				<li class="li_2"
					style="display: none; background: url(./images/2.jpg) 50% 0% no-repeat;">
					<div class="banner_main_con">
						<a href="#" target="_blank" class="main_banner_a"></a>
					</div>
				</li>


				<li class="li_3"
					style="display: none; background: url(./images/3.jpg) 50% 0% no-repeat;">
					<div class="banner_main_con">
						<a href="#" target="_blank" class="main_banner_a"></a>
					</div>
				</li>


				<li class="li_4"
					style="display: none; background: url(./images/4.jpg) 50% 0% no-repeat;">
					<div class="banner_main_con">
						<a href="#" target="_blank" class="main_banner_a"></a>
					</div>
				</li>




			</ul>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			jQuery(".slideBox").slide({mainCell:".bd ul",autoPlay:true,delayTime:700});
			
			$(".mall_banner_ad").find("a").each(function(){
				$(this).hover(function(){
					$(this).stop().animate({left:-5},"200");
				},function(){
					$(this).stop().animate({left:0},"200");
				});
			});
		});
		
		
		
</script>


	<div class="w" style="position:relative;">
		<div style="position:absolute; right:0px; top:-480px; width:250px;">
			<div id="jdnews" class="m m1" style="margin-top:0px;">

				<div class="mt">
					<h2>快讯</h2>
					<div class="extra">
						<a href="#" target="_blank">更多&nbsp;&gt;</a>
					</div>
				</div>
				<div class="mc">
					<ul>
						<li><a href="#" title="【最新公告】有关2016年最新借书说明">【最新公告】有关2016年图书漂流活动通知</a></li>

					</ul>
				</div>
			</div>
			<div class="m _520_a_lifeandjourney_1 hover" id="virtuals-2014"
				style="height:277px"></div>
		</div>
	</div>



	<div class="clear"></div>
	<div style="width:1210px;margin:5px auto;padding:0 auto;">
		<table cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td><a href="#" target="_blank"><img
							src="./images/1439030005283589829.jpg" width="1210" height="120"
							border="0"></a></td>
				</tr>
			</tbody>
		</table>
		<br> <br>
		<div id="floors-list">

			<div class="w catGoodsBox catGoodsSlide on"></div>

		</div>



	</div>

	</div>




	</div>
	</div>
	</div>
	<span class="clr"></span>
	</div>
	<div style="width:1210px;margin:5px auto;padding:0 auto;">




		<div class="w">

			<div class="w_100_l">
				<div class="main">


					<p class="index_hr"></p>


					<fieldset>

						<div class="form-row-select">


							<div class="content">
								<h1 class="h1_book_title"></h1>
								<ul>

									<s:iterator value="Book" id="b">
										<li>
											<dl>
												<dd>
													<img src="images/book/<s:property value="#b.bpic" />"
														alt="book" />
												</dd>
												<dt>
													<p class="book_title">
														书名：
														<s:property value="#b.bname" />
													</p>
													<p class="book_inline">
														作者：
														<s:property value="#b.bauthor" />
													</p>
													<input type="button" value="详情"
														id=<s:property value="#b.bid"/>
														onclick="searchClick3(this.id)">

												</dt>
											</dl>
										</li>

									</s:iterator>

								</ul>
								<script type="text/javascript">
  	                  function searchClick3(id){  	                  
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?Bookid="+id
  	                  ;
  	                  }
  	                  </script>
							</div>


							<%String pagenum=(String)request.getAttribute("PageNum");
      String keyword=(String)request.getAttribute("keyword");  
      String type=(String)request.getAttribute("type");          
         try {
          int pagen=Integer.parseInt(pagenum);
          for(int i=0;i<pagen;i++){ %>
			<div style="margin-left:30%">
							<fieldset style="margin-top:82px;text-align:center;">
								<input type="button" value="<%=i+1%>" name="<%=keyword%>"
									id="<%=type%>"
									onclick="searchClick4(this.value,this.name,this.id)">
							</fieldset>
</div>
							<script type="text/javascript">
  	                  function searchClick4(value,keyword,type){  	                  
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?Page="+value+"&Keyword="+keyword+"&type="+type;
  	                  }
  	                  </script>

							<%         
          }
         } catch (NumberFormatException e) {
         e.printStackTrace();
         }         
          %>
						
					</fieldset>

					<p class="index_hr"></p>

					<div class="footer" style="margin-left:22%;">
						<span class="span_1">&copy; Copyright 2016, Beijing
							Jiaotong University</span>&nbsp;&nbsp; <a href="#">Help & Contact us</a>
					</div>
				</div>
			</div>

		</div>

	</div>
	</div>
</body>
</html>