<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<html>
<head>
<title>图书漂流-图书发布</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"href="images/head.ico"/>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
	function setActiveStyleSheet(title) {
		var i, a, main;
		for (i = 0; (a = document.getElementsByTagName("link")[i]); i++) {
			if (a.getAttribute("rel").indexOf("style") != -1
					&& a.getAttribute("title")) {
				a.disabled = true;
				if (a.getAttribute("title") == title)
					a.disabled = false;
			}
		}
	}
</script>
<link rel="stylesheet" type="text/css" href="css/publish-style.css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.doubleSelect.js"></script>
<script type="text/JavaScript">
	$(document).ready(function() {
		var selectoptions = {
			
                "计算机与软件类" : {
				"key" : 1,
				"defaultvalue" : 47,
				"values" : {
					"Android" : 1,
					"Java": 2,
					"IOS" : 3,
					"JavaScript" : 4,
					"PHP" : 5,
					"C++" : 6,
					"C#" : 7,
                    "HTML5" : 8,
                    "JSP" : 9,
                    "C语言" : 10,
                    "Excel" : 11,
                    "PPT" : 12,
 
	                "Word": 13,
					"Office集合": 14,
					"DreamWeaver" : 15,
					"Flash" : 16,
					"Fireworks" : 17,
					"Authorware" : 18,
					"CSS" : 19,
                    "FrantPage" : 20,
                    "网络组建" : 21,
                    "网络协议" : 22,
                    "网络管理" : 23,
                    "网络理论" : 24,
                    "WebServer": 25,
                    "SQL语言": 26,
					"Oracle": 27,
					"MySQL" : 28,
					"Linux" : 29,
					"Unix" : 30,
					"MacOS" : 31,
					"Windows" : 32,
                    "DOS" : 33,
                    "系统开发" : 34,
                    "PhotoShop" : 35,
                    "3dsMAX" : 36,
                    "Premiere" : 37,
 
                    "MAYA": 38,
                    "Painter": 39,
					"Freehand": 40,
					"机器学习" : 41,
					"软件工程" : 42,
					"计算机安全" : 43,
					"电子商务" : 44,
					"移动开发" : 45,
                    "大数据与云计算" : 46,                                      
				}
			},


                "经济管理类" : {
				"key" : 2,
				"defaultvalue" : 25,
				"values" : {
					"市场营销" : 1,
					"企业管理": 2,
					"团队建设" : 3,
					"财务管理" : 4,
					"人力资源" : 5,
					"供应链" : 6,
					"经济史" : 7,
                                          "会计/审计" : 8,
                                             "国际贸易" : 9,
                                             "国际金融" : 10,
                                          "互联网金融" : 11,
                                              "中国经济" : 12,
 
	                                "世界经济": 13,
					"财政税收": 14,
					"股票" : 15,
					"期权" : 16,
					"期货" : 17,
					"基金" : 18,
					"大数据" : 19,
                                          "网络营销" : 20,
                                             "客户服务" : 21,
                                             "搜索优化" : 22,
                                          "电商思维" : 23,
                                              "互联网+" : 24,
                                        
				
                                      




				}
			},


                            "电信类" : {
				"key" : 3,
				"defaultvalue" : 24,
				"values" : {
					"模拟电子技术" : 1,
					"数字电子技术": 2,
					"微机原理与接口" : 3,
					"信息论基础" : 4,
					"计算机网络与通信" : 5,
					"嵌入式系统" : 6,
					"EDA技术" : 7,
                                          "信号与系统" : 8,
                                             "模拟电子线路" : 9,
                                             "数字电子线路" : 10,
                                          "精密机械与仪器设计" : 11,
                                              "精密机械制造工程" : 12,
 
	                                "微型计算机原理与应用": 13,
					"热工学": 14,
					"工程材料与热处理" : 15,
					"电工与电子技术" : 16,
					"电力服务系统" : 17,
					"TCP/IP协议" : 18,
					"物联网技术" : 19,
                                          "数字信号处理" : 20,
                                             "信号与系统" : 21,
                                             "RFID原理与应用" : 22,
                                          "通信电子线路" : 23,
                                             
                                        
				
                                      




				}
			},

                            "交通运输类" : {
				"key" : 4,
				"defaultvalue" : 26,
				"values" : {
					"工程图学" : 1,
					"汽车构造": 2,
					"机械设计" : 3,
					"交通运输设备" : 4,
					"交通运输法规" : 5,
					"交通规划" : 6,
					"理论力学" : 7,
                                          "汽车设计" : 8,
                                             "汽车制造工艺学" : 9,
                                             "运输市场营销" : 10,
                                          "交通港站与枢纽" : 11,
                                              "材料力学" : 12,
 
	                                "汽车理论": 13,
					"机械原理": 14,
					"城市轨道交通客运管理" : 15,
					"结构力学" : 16,
					"热工与发动机原理" : 17,
					"道路建筑材料" : 18,
					"轨道交通信号原理" : 19,
                                          "交通运输组织学" : 20,
                                             "测量学" : 21,
                                             "液压与气压传动" : 22,
                                          "智能交通" : 23,
                                             
                            
	                                "交通控制与管理": 24,
					"现代物流学": 25,
					
                                      
				
                                      




				}
			},

                            "土木工程类" : {
				"key" : 5,
				"defaultvalue" : 21,
				"values" : {
					"基础工程(道桥)" : 1,
					"线路工程": 2,
					"工程项目管理" : 3,
					"结构设计原理(钢结构)" : 4,
					"结构抗震及高层建筑" : 5,
					"土木工程项目管理" : 6,
					"钢桥" : 7,
                                          "工程合同管理" : 8,
                                             "桥渡设计" : 9,
                                             "工程流体力学" : 10,
                                          "桥隧工程" : 11,
                                              "投资风险管理学" : 12,
 
	                                "工程数学": 13,
					"道路勘测设计": 14,
					"工程监理" : 15,
					"建设与房地产法规" : 16,
					"工程估价" : 17,
					"路基路面工程" : 18,
					"建筑设备" : 19,
                                          "工程承包与招投标" : 20,
                                            
                                      
				
                                      




				}
			},

                           "电气工程类" : {
				"key" : 6,
				"defaultvalue" : 13,
				"values" : {
					"电路原理" : 1,
					"电机学": 2,
					"信号与系统" : 3,
					"电力系统分析" : 4,
					"检测与转换技术" : 5,
					"工程制图" : 6,
					"电子技术基础" : 7,
                                          "电力电子技术" : 8,
                                             "自动控制理论" : 9,
                                             "电气控制技术" : 10,
                                          "电力系统继电保护" : 11,
                                              "电机学" : 12,
 
	                               
                                      
				
                                      




				}
			},



                           "机电类" : {
				"key" : 7,
				"defaultvalue" : 19,
				"values" : {
					"材料物理化学" : 1,
					"液压传动": 2,
					"机械设计学" : 3,
					"机械系统设计" : 4,
					"机械动力学" : 5,
					"汽车构造" : 6,
					"发动机原理" : 7,
                                          "汽车理论" : 8,
                                             "电力电子技术" : 9,
                                             "智能仪器" : 10,
                                          "虚拟仪器" : 11,
                                              "工程力学" : 12,
 
	                     		"传感器技术" : 13,
					"物联网技术与应用": 14,
					"工业工程基础" : 15,
					"企业信息化" : 16,
					"生产管理与控制" : 17,
					"人机工程学" : 18,
					
	                          
                                      
				
                                      




				}
			},


                           "理学类" : {
				"key" : 8,
				"defaultvalue" : 14,
				"values" : {
					"数学分析" : 1,
					"数学模型": 2,
					"金融数学" : 3,
					"数据分析系统教程" : 4,
					"金融计量经济学" : 5,
					"常微分方程" : 6,
					"微观经济学" : 7,
                                          "宏观经济学" : 8,
                                             "信息论与编码" : 9,
                                             "离散数学" : 10,
                                          "图形图像处理技术" : 11,
                                              "计算机图形学" : 12,
 
	                     		"数值分析" : 13,
					
	                          
                                      
				
                                      




				}
			},


                           "语言与传播类" : {
				"key" : 9,
				"defaultvalue" : 10,
				"values" : {
					"综合英语" : 1,
					"高级英语": 2,
					"英语口语" : 3,
					"写作" : 4,
					"听力" : 5,
					"新闻学概论" : 6,
					"新闻采访与写作" : 7,
                                          "大众传播学" : 8,
                                             "广告学" : 9,
                                            
	                          
                                      
				
                                      




				}
			},


                           "建筑与艺术类" : {
				"key" : 10,
				"defaultvalue" : 20,
				"values" : {
					"建筑力学" : 1,
					"建筑材料": 2,
					"建筑构造" : 3,
					"中外建筑史" : 4,
					"公共建筑设计原理" : 5,
					"室内设计" : 6,
					"城市设计概论" : 7,
                                          "中外美术史" : 8,
                                             "设计基础" : 9,
                                            
	              			"艺术设计史" : 10,
					"设计素描": 11,
					"版式设计" : 12,
					"植物学" : 13,
					"景观生态学" : 14,
					"中国古典园林史" : 15,
					"城乡规划原理" : 16,
                                          "城乡规划设计" : 17,
                                             "城市生态环境保护" : 18,
                 	              	"区域规划" : 19,
					
                                      				                                      
				}
			},


                "法学类" : {
				"key" : 11,
				"defaultvalue" : 10,
				"values" : {
					 "理论法学" : 1,
					 "法理学": 2,
					 "法哲学" : 3,
					 "法律社会学" : 4,
					 "法律逻辑学" : 5,
				   	 "立法学" : 6,
				     "社会研究方法" : 7,
                     "公共关系学" : 8,
                     "管理心理学" : 9,                                           	              								                                     				                                  
				}
			},
                "基础课程" : {
				"key" : 12,
				"defaultvalue" : 5,
				"values" : {
					"线性代数" : 1,
					"微积分": 2,
					"毛泽东思想概论" : 3,
					"马克思主义哲学" : 4,										                                     				                                    
				}
			}
		};
		$('#first').doubleSelect('second', selectoptions);
	});
</script>
</head>
<body style="background:url(images/bg.gif) repeat;">
	<div class="form_content">
		<form id="test" action="BorrowAction" method="post"
			accept-charset="utf-8" enctype="multipart/form-data">
			<fieldset style="width:95%;">
				<legend>图书信息</legend>
				<div class="form-row" style="width:70%; margin-left:15%;">
					<div class="field-label">
						<label for="bookname">图书名:</label>
					</div>
					<div class="field-widget">
						<input name="bookname" id="bookname" class="required"
							title="Enter book name" />
					</div>
				</div>
				<div class="form-row" style="width:70%; margin-left:15%;">
					<div class="field-label">
						<label for="authorname">作者名</label>:
					</div>
					<div class="field-widget">
						<input name="authorname" id="authorname" class="required"
							title="Enter author name" />
					</div>
				</div>
				<div class="form-row" style="width:70%; margin-left:15%;">
					<div class="field-label">
						<label for="publishername">出版社</label>:
					</div>
					<div class="field-widget">
						<input name="publishername" id="publishername" class="required"
							title="Enter publish name" />
					</div>
				</div>
				<!--  <div class="form-row">
            <div class="field-label"><label for="field3">详细描述</label>:</div>
            <div class="field-widget"><textarea class="required"></textarea></div>
        </div> -->
			</fieldset>
			<fieldset style="width:95%;">
				<legend>上传照片</legend>
				<div class="left">
					<div id='edit' style="margin-top: 140px;"></div>
					</section>
					<h2></h2>
					<div id="preview" style="margin-left: 80px;">
						<img id="imghead" />
					</div>
					<section style="float: right;margin-top: -160px;margin-right: 3%;">
						<input type="file" onchange="previewImage(this)"
							style="width: 200px;height: 70px;opacity: 0;-moz-opacity: 0; -webkit-opacity: 0;"
							name="file" id="file" />
						<section
							style="width: 200px;height: 60px;margin-top: -70px;background: #134364;color: #fff;border-radius: 14px;text-align: center;line-height: 60px;">
							<a>上传图片</a>
						</section>
					</section>
					<section style="float: right;margin-top: -80px;margin-right: 3%;">
						<a style="color: #f48b0e;">1、图片尺寸不小于400*400，大小不超过500K</a><br /> <a
							style="color: #f48b0e;">2、图片尽量清晰</a>
					</section>
				</div>
			</fieldset>
			<fieldset style="width:95%;">
				<legend class="optional">类别</legend>
				<div class="form-row" style="width:100%;">
					<div class="field-label">
						<label for="field6">分类</label>:
					</div>
					<div class="field-widget"  style="margin-left:10%;width:80%;">
						
							<fieldset>

								<div>
									<label for="first"> 一级分类 : </label> <select id="first"
										name="first" size="1"><option value="">--</option></select>
								</div>
								<br />
								<div>
									<label for="second"> 二级分类 : </label> <select id="second"
										name="second" size="1"><option value="">--</option></select>
								</div>
							</fieldset>
						
					</div>
				</div>
				<div class="form-row-select">
					
				</div>
			</fieldset>
			<fieldset style="width:95%;">
						<legend class="optional">选项</legend>
						<div style="margin-left:40%;">
						<label class="left"> <input type="radio"
							class="radio_input" name="require" id="require" value="1" />借出
							<br/>
						</label> <label class="left"> <input type="radio"
							class="radio_input" name="require" id="require" value="2" />借入<br />
						</label>
						</div>
					</fieldset>
			<input type="submit" class="submit" value="确认"
				style="margin-left:45%;width:10%;" />
		</form>
	</div>


	<script type="text/javascript">
		function previewImage(file) {
			var MAXWIDTH = 200;
			var MAXHEIGHT = 200;
			var div = document.getElementById('preview');
			if (file.files && file.files[0]) {
				div.innerHTML = '<img id=imghead>';
				var img = document.getElementById('imghead');
				img.onload = function() {
					var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT,
							img.offsetWidth, img.offsetHeight);
					img.width = rect.width;
					img.height = rect.height;
					//                 img.style.marginLeft = rect.left+'px';
					img.style.marginTop = rect.top + 'px';
				}
				var reader = new FileReader();
				reader.onload = function(evt) {
					img.src = evt.target.result;
				}
				reader.readAsDataURL(file.files[0]);
			} else //兼容IE
			{
				var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
				file.select();
				var src = document.selection.createRange().text;
				div.innerHTML = '<img id=imghead>';
				var img = document.getElementById('imghead');
				img.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
				var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT,
						img.offsetWidth, img.offsetHeight);
				status = ('rect:' + rect.top + ',' + rect.left + ','
						+ rect.width + ',' + rect.height);
				div.innerHTML = "<div id=divhead style='width:" + rect.width + "px;height:" + rect.height + "px;margin-top:" + rect.top + "px;" + sFilter + src + "\"'></div>";
			}
		}

		function clacImgZoomParam(maxWidth, maxHeight, width, height) {
			var param = {
				top : 0,
				left : 0,
				width : width,
				height : height
			};
			if (width > maxWidth || height > maxHeight) {
				rateWidth = width / maxWidth;
				rateHeight = height / maxHeight;
				if (rateWidth > rateHeight) {
					param.width = maxWidth;
					param.height = Math.round(height / rateWidth);
				} else {
					param.width = Math.round(width / rateHeight);
					param.height = maxHeight;
				}
			}
			param.left = Math.round((maxWidth - param.width) / 2);
			param.top = Math.round((maxHeight - param.height) / 2);
			return param;
		}
		$(function() {
			$('#edit').editable({
				inlineMode : false,
				alwaysBlank : true
			})
		});
	</script>
	</div>
</body>
</html>