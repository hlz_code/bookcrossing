<%@ page language="java" import="java.util.*" pageEncoding="utf8"%>
<html>
<head>
<title>图书漂流-登陆</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
<link href="css/login2.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon"href="images/head.ico"/>
</head>
<body style="background: #fff url(./images/back.jpg) no-repeat;width:100%; height:100%;">
	<h1>登陆</h1>

	<div class="login" style="margin-top:50px;">

		<div class="header">
			<div class="" id="switch">
				<label class="switch_btn_focus" id="switch_qlogin"
					href="javascript:void(0);" tabindex="7"
					style="margin-left:38%;width:24%;font-size:25px;"
					style="margin-left:38%;width:24%;font-size:25px;">快速登录</label>
			</div>
		</div>


		<div class="web_qr_login" id="web_qr_login"
			style="display: block; height: 235px;">

			<!--登录-->
			<div class="web_login" id="web_login">


				<div class="login-box">


					<div class="login_form">
						<form action="LoginAction" name="loginform"
							accept-charset="utf-8" id="login_form" class="loginForm"
							method="post">
							<input type="hidden" name="did" value="0" /> <input type="hidden"
								name="to" value="log" />
							<div class="uinArea" id="uinArea">
								<label class="input-tips" for="u">帐号：</label>
								<div class="inputOuter" id="uArea">

									<input type="text" id="u" name="username" class="inputstyle" required="required"/>
								</div>
							</div>
							<div class="pwdArea" id="pwdArea">
								<label class="input-tips" for="p">密码：</label>
								<div class="inputOuter" id="pArea">

									<input type="password" id="p" name="password" class="inputstyle" required="required"/>
								</div>
							</div>

							<div style="padding-left:50px;margin-top:20px;">
								<input type="submit" value="登 录" style="width:150px;"
									class="button_blue" /><a href="register.jsp" class="zcxy" target="_blank">注册</a>
							</div>
							

							
						</form>
					</div>

				</div>

			</div>
			<!--登录end-->
		</div>


	</div>
</body>
</html>