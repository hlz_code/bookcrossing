<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<head>
<title>图书漂流-注册</title>
<link rel="shortcut icon"href="images/head.ico"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script style="text/javascript">
/*function checkMobile(str) {
    var re = /^1\d{10}$/;
    if (re.test(str)) {
        return true;
    } else {
        return false;
    }
}
*/
	function subForm() {
		var pw1 = document.getElementById("passwd");
		var pw2 = document.getElementById("passwd2");
		var uname=document.getElementById("user");
		var uphone=document.getElementById("phone");
		
		if(uname.value==null||uname.value==""){
		alert("账号不能为空！");
		return false;
		}
		if(pw1.value==""||pw2.value==""){
		alert("密码不能为空!");
		return false;
		}
		if (pw1.value != pw2.value){
			alert("您两次输入密码不一致!");
			return false;
		}
		if(uphone.value==""){
		alert("手机号不能为空!");
		return false;
		} 
		/*if(checkMobile(uphone)==false){
		alert("电话号码格式有误!");
		return false;
		}*/
		else {
			document.getElementById("RegisterForm").submit();

		}
	}
</script>
<link href="css/login2.css" rel="stylesheet" type="text/css" />
</head>
<body style="background: #fff url(./images/back.jpg) no-repeat;width:100%; height:100%;">
	<form id="RegisterForm" action="RegisterAction" method="post" accept-charset="utf-8">
		<h1>校园图书漂流</h1>

		<div class="login" style="margin-top:50px;">

			<div class="header">
				<div class="" id="switch">
					<label class="switch_btn_focus" id="switch_login"
						href="javascript:void(0);" tabindex="8"
						style="margin-left:38%;width:24%;font-size:25px;">快速注册</label>
				</div>
			</div>


			<!--注册-->
			<div class="qlogin" id="qlogin">

				<div class="web_login">
						<input type="hidden" name="to" value="reg" /> <input
							type="hidden" name="did" value="0" />
						<ul class="reg_form" id="reg-ul">
							<div id="userCue" class="cue">快速注册请注意格式</div>
							<li><label for="user" class="input-tips2">用户名：</label>
								<div class="inputOuter2">
									<input type="text" id="user" name="name" maxlength="16"
										class="inputstyle2" required="required" />
								</div></li>


							<li><label for="passwd" class="input-tips2">密码：</label>
								<div class="inputOuter2">
									<input type="password" id="passwd" name="passwd" maxlength="16"
										class="inputstyle2" required="required" />
								</div></li>

							<li><label for="passwd" class="input-tips2">确认密码：</label>
								<div class="inputOuter2">
									<input type="password" id="passwd2" name="passwd2"
										maxlength="16" class="inputstyle2" required="required" /> <br />
									<span id="spantips"></span>
								</div></li>



							<li><label for="qq" class="input-tips2">电话号码：</label>
								<div class="inputOuter2">
									<input type="text" id="phone" name="phone" maxlength="11"
										class="inputstyle2" required="required" />
								</div></li>


							<div class="inputArea">
								<input type="button" id="reg"
									style="margin-top:10px;margin-left:85px;" class="button_blue"
									onclick="subForm();" value="同意协议并注册" /> <a href="#"
									class="zcxy" target="_blank">注册协议</a>
							</div>

							</li>
							<div class="cl"></div>
						</ul>
					</form>
				</div>
			</div>
			<!--注册end-->
		</div>
	</form>
</body>
</html>