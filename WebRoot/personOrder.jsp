<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<head>
<title>图书漂流-我的个人中心</title>
<link href="css/personinfo/bootstrap.css" rel='stylesheet'
	type='text/css' />

<!-- Custom Theme files -->
<link href="css/personinfo/dashboard.css" rel="stylesheet">
<link href="css/personinfo/style.css" rel='stylesheet' type='text/css' />
<link rel="shortcut icon"href="images/head.ico"/>
<script charset="utf-8"
	src="http://map.qq.com/api/js?v=2.exp?key=YPIBZ-XSC3V-RSSPA-UYPAS-DJ5GE-3KFHU&referer=bookcrossing"></script>
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


				<style type="text/css">

/*
	Pretty Table Styling
	CSS Tricks also has a nice writeup: http://css-tricks.com/feature-table-design/
	*/
table {
	overflow: hidden;
	border: 1px solid #d3d3d3;
	background: #fefefe;
	width: 70%;
	margin: 5% auto 0;
	-moz-border-radius: 5px; /* FF1+ */
	-webkit-border-radius: 5px; /* Saf3-4 */
	border-radius: 5px;
	-moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
	-webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
}

th, td {
	padding: 18px 28px 18px;
	text-align: center;
}

th {
	padding-top: 22px;
	text-shadow: 1px 1px 1px #fff;
	background: #e8eaeb;
}

td {
	border-top: 1px solid #e0e0e0;
	border-right: 1px solid #e0e0e0;
}

tr.odd-row td {
	background: #f6f6f6;
}

td.first, th.first {
	text-align: left
}

td.last {
	border-right: none;
}

/*
	Background gradients are completely unnecessary but a neat effect.
	*/
td {
	background: -moz-linear-gradient(100% 25% 90deg, #fefefe, #f9f9f9);
	background: -webkit-gradient(linear, 0% 0%, 0% 25%, from(#f9f9f9),
		to(#fefefe));
}

tr.odd-row td {
	background: -moz-linear-gradient(100% 25% 90deg, #f6f6f6, #f1f1f1);
	background: -webkit-gradient(linear, 0% 0%, 0% 25%, from(#f1f1f1),
		to(#f6f6f6));
}

th {
	background: -moz-linear-gradient(100% 20% 90deg, #e8eaeb, #ededed);
	background: -webkit-gradient(linear, 0% 0%, 0% 20%, from(#ededed),
		to(#e8eaeb));
}

/*
	I know this is annoying, but we need additional styling so webkit will recognize rounded corners on background elements.
	Nice write up of this issue: http://www.onenaught.com/posts/266/css-inner-elements-breaking-border-radius
	
	And, since we've applied the background colors to td/th element because of IE, Gecko browsers also need it.
	*/
tr:first-child th.first {
	-moz-border-radius-topleft: 5px;
	-webkit-border-top-left-radius: 5px; /* Saf3-4 */
}

tr:first-child th.last {
	-moz-border-radius-topright: 5px;
	-webkit-border-top-right-radius: 5px; /* Saf3-4 */
}

tr:last-child td.first {
	-moz-border-radius-bottomleft: 5px;
	-webkit-border-bottom-left-radius: 5px; /* Saf3-4 */
}

tr:last-child td.last {
	-moz-border-radius-bottomright: 5px;
	-webkit-border-bottom-right-radius: 5px; /* Saf3-4 */
}
</style>
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- start menu -->

</head>
<body onload="init()">
	<s:iterator value="list" id="u">
		<!-- header -->
		<div class="col-sm-3 col-md-2 sidebar">
			<div class="sidebar_top">
				<h1></h1>
				<a href='http://localhost:8080/bookcrossing/editHeader.jsp'
					target="_blank"><img src="<s:property value="#u.upic" />"
					alt="" /></a>
			</div>
			<div class="details">
				<h3>用户名</h3>
				<p>
					<s:property value="#u.uname" />
				</p>
				<h3>电&nbsp;&nbsp;&nbsp;话</h3>
				<p>
					<s:property value="#u.uphone" />
				</p>
				<h3>邮&nbsp;&nbsp;&nbsp;箱</h3>
				<p>
					<a href="mailto@example.com"><s:property value="#u.uemail" /></a>
				</p>
				<address>
					<h3>学&nbsp;&nbsp;&nbsp;校</h3>
					<p>
						<s:property value="#u.uschool" />
					</p>
				</address>

			</div>
			<div class="clearfix"></div>
		</div>
		<!---->
		<link href="css/personinfo/popuo-box.css" rel="stylesheet"
			type="text/css" media="all" />
		<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<!---//pop-up-box---->
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="content">
				<div class="details_header">
					<ul>
						<li><a target="_blank" href="http://localhost:8080/bookcrossing/PersonCenterAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>信息详情</a></li>
						<li><a target="_blank"
							href="http://localhost:8080/bookcrossing/ModPersonCenterAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>修改信息</a></li>
						<li><a target="_blank"
							href="http://localhost:8080/bookcrossing/PersonPublishAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>我的发布</a></li>
						<li><a target="_blank" href="http://localhost:8080/bookcrossing/PersonOrderAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>我的订单</a></li>
					</ul>
				</div>
				<div style="width:100%;">
					<div class="company" >
						<h3 class="clr1">我的订单</h3>
						<div class="form-row-select" style="margin-left:2%;">
						<table>
						<thead>
						<tr>
						<th style="width:20%;">编号</th>
						<th style="width:20%;">书名</th>
						<th style="width:20%;">时间</th>
						<th style="width:20%;">状态</th></tr>
						</thead>
						<tbody>
						<s:iterator value="Order" id="o" status="index">
						<tr>
						<td style="width:20%;"><s:property value="#index.index+1"/></td>
						<td style="width:20%;"><s:property value="#o.bid" /></td>
						<td style="width:20%;"><s:property value="#o.otime" /></td>
						<td id="orderstatus" style="width:20%;"></td>
						<script>
						var val = '<s:property value="#o.ostatusin" />';
						if(val == 0){
						document.getElementById("orderstatus").innerHTML='等待对方同意';
						}
						if(val == 1){
						document.getElementById("orderstatus").innerHTML='对方已拒绝';
						}
						if(val == 2){
						document.getElementById("orderstatus").innerHTML='对方已同意';
						}
						</script>
						</tr>
						</s:iterator>
						</tbody>
						</table>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!---->
	</s:iterator>
</body>
</html>