<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="model.Book" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<title>图书漂流</title>
<link type="text/css" rel="stylesheet" href="css/reset.css" />
<link type="text/css" rel="stylesheet" href="css/1024_768.css" />
<link type="text/css" rel="stylesheet" media="screen and (min-width:861px) and (max-width:960px)" href="css/pad_heng.css" />
<link type="text/css" rel="stylesheet" media="screen and (min-width:601px) and (max-width:860px)" href="css/pad.css" />
<link type="text/css" rel="stylesheet" media="screen and (min-width:481px) and (max-width:600px)" href="css/tel_heng.css" />
<link type="text/css" rel="stylesheet" media="screen and (max-width:480px)" href="css/tel.css" />
<link rel="stylesheet" type="text/css" href="css/publish-style.css" />
<link rel="shortcut icon"href="images/head.ico"/>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.doubleSelect.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/JavaScript">
	$(document).ready(function() {
		var selectoptions = {
			"计算机" : {
				"key" : 10,
				"defaultvalue" : 117,
				"values" : {
					"硬件/外部设备/维修" : 110,
					"操作系统" : 111,
					"数据库" : 112,
					"图形图像/多媒体" : 113,
					"CAD/CAM/CAE" : 114,
					"认证/等级考试" : 115,
					"信息安全" : 116,
					"编程语言" : 117,
				}
			},
			"教学用书" : {
				"key" : 20,
				"defaultvalue" : 220,
				"values" : {
					"电信" : 210,
					"计科" : 211,
					"经管" : 212,
					"交运" : 213,
					"土建" : 214,
					"机电" : 215,
					"电气" : 216,
					"理学院" : 217,
					"马克思" : 218,
					"语传" : 219,
					"软件" : 220,
					"建艺" : 221,
					"法学" : 222,

				}
			},
			"小说" : {
				"key" : 30,
				"defaultvalue" : 312,
				"values" : {
					"世界名著" : 310,
					"外国小说" : 311,
					"中国古典小说" : 312,
					"悬疑推理小说" : 313,
					"恐怖小说" : 314,
					"武侠小说" : 315,
					"言情小说" : 316,
				}
			}
		};
		$('#first').doubleSelect('second', selectoptions);
	});
</script>
</head>

<body>
<div class="w_100_l">
	<div class="main">
      <div class="top_banner">
            <div class="top_logo"><img src="images/top_logo.jpg" alt="A BOOK APART LOGO" /></div>
            <div class="top_menu">
            	<ul>
                	<li class="sel"><a href="#">HOME</a></li>
                	<li><a href="#">STORE</a></li>
                	<li><a href="#">PRESS</a></li>
                	<li><a href="#">ABOUT</a></li>
                	<li><a href="#">HELP</a></li>
                </ul>
            </div>
            
             <%String username=(String)session.getAttribute("username");
             if(username==null){%>
              <div class="top_shop_cur"><a href="http://localhost:8080/bookcrossing/login.jsp">请登录!</a></div>            
             <% 
             }else{  
            %>
            <div class="top_shop_cur"><a href="#">欢迎!  <%=username%></a>
            <a href="publish.jsp">publish</a>
            </div>
            
            <%}
             %>
            
        </div>
               
        <span class="index_img"><img src="images/banner_img.jpg" alt="Dan Cederholm" border="0" usemap="#Map" />
        <map name="Map" id="Map">
          <area shape="rect" coords="857,230,930,269" href="#" alt="buy now" />
        </map>
        </span>
        <p class="index_hr"></p>
       
		
		<fieldset>
				<legend class="optional">类别</legend>
				<div class="form-row" style="width:100%;">
				<fieldset style="width: 45%;float:left;margin-left: 2%;">
					<div class="field-label">
						<label for="field6">分类</label>:
					</div>
					<div class="field-widget">																				
									<select id="first"
										name="first" size="1"><option value="">--</option></select>								
									<select id="second"
										name="second" size="1"><option value="">--</option></select>																			
					</div>
					<div class="field-widget">						
					<img src="images/query.png"  onclick="searchClick()"/>
					 <script type="text/javascript">
  	                  function searchClick(){
  	                  var first = document.getElementById("first").value;
  	                  var second = document.getElementById("second").value;
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?type="+first+"-"+second;
  	                  }
  	                  </script>
					</div>
					</fieldset>
				   
					
					
					<fieldset style="width: 45%;float:left;">
					<div class="field-label">
						<label for="field6">查询</label>:						
					</div>
					<div class="field-widget">	
					<input type="text" id="u" name="Keyword"/>
					</div>
					<div class="field-widget">	
					<img src="images/query.png" onclick="searchClick2()"/>
					<script type="text/javascript">
  	                  function searchClick2(){
  	                  var u = document.getElementById("u").value;
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?Keyword="+u;
  	                  }
  	                  </script>
					</div>
					</fieldset>
				</div>
				<div class="form-row-select">
		        
		
      <div class="content">
            <h1 class="h1_book_title">书籍如下：</h1>
        	<ul>        	
        	<s:iterator value="Book" id="b">
            	<li>
                	<dl>
                    	<dd><img src="images/book/<s:property value="#b.bpic" />" alt="book" /></dd>
                        <dt>
                        	<p class="book_title">书名：<s:property value="#b.bname" /></p>
                            <p class="book_inline">作者： <s:property value="#b.bauthor" /></p>
                           <input type="button"  value="详情" id=<s:property value="#b.bid"/>  onclick="searchClick3(this.id)">
                           
                        </dt>
                    </dl>
                </li>
                
              </s:iterator>                                       	
            </ul>
           <script type="text/javascript">
  	                  function searchClick3(id){  	                  
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?Bookid="+id
  	                  ;
  	                  }
  	                  </script>
      </div>
      
     
      <%String pagenum=(String)request.getAttribute("PageNum");
      String keyword=(String)request.getAttribute("keyword");  
      String type=(String)request.getAttribute("type");          
         try {
          int pagen=Integer.parseInt(pagenum);
          for(int i=0;i<pagen;i++){ %>
          
          <fieldset style="margin-top:82px;text-align:center;">
          <input type="button" value="<%=i+1%>" name="<%=keyword%>" id="<%=type%>" onclick="searchClick4(this.value,this.name,this.id)"> 
          </fieldset>
                 
                <script type="text/javascript">
  	                  function searchClick4(value,keyword,type){  	                  
  	                  window.location.href="http://localhost:8080/bookcrossing/SearchAction?Page="+value+"&Keyword="+keyword+"&type="+type;
  	                  }
  	                  </script>  
  	                     
          <%         
          }
         } catch (NumberFormatException e) {
         e.printStackTrace();
         }         
          %> 
           
          
                    
      
	  </fieldset>
	
         
	  
        <p class="index_hr"></p>
        
        <div class="footer">
           <span class="span_1">&copy; Copyright 2014, A Book Apart, LLC</span>&nbsp;&nbsp;
            <a href="#">Help & Contact us</a>
            <a class="a1" href="#">Press Room RSS feed</a>
            <a class="a2" href="#">abookapart on Twitter</a>
            <span class="span_2"><b>Published by</b><img src="images/icon_hg.jpg" align="absmiddle" /></span>
        </div>
    </div>
</div>

</body>
</html>
