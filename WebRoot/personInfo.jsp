<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<head>
<title>图书漂流-我的个人中心</title>
<link href="css/personinfo/bootstrap.css" rel='stylesheet'
	type='text/css' />

<!-- Custom Theme files -->
<link href="css/personinfo/dashboard.css" rel="stylesheet">
<link href="css/personinfo/style.css" rel='stylesheet' type='text/css' />
<link rel="shortcut icon"href="images/head.ico"/>
<script charset="utf-8"
	src="http://map.qq.com/api/js?v=2.exp?key=YPIBZ-XSC3V-RSSPA-UYPAS-DJ5GE-3KFHU&referer=bookcrossing"></script>
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- start menu -->

</head>
<body onload="init()">
	<s:iterator value="list" id="u">
		<!-- header -->
		<div class="col-sm-3 col-md-2 sidebar">
			<div class="sidebar_top">
				<h1></h1>
				<a href='http://localhost:8080/bookcrossing/editHeader.jsp'
					target="_blank"><img src="<s:property value="#u.upic" />"
					alt="" /></a>
			</div>
			<div class="details">
				<h3>用户名</h3>
				<p>
					<s:property value="#u.uname" />
				</p>
				<h3>电&nbsp;&nbsp;&nbsp;话</h3>
				<p>
					<s:property value="#u.uphone" />
				</p>
				<h3>邮&nbsp;&nbsp;&nbsp;箱</h3>
				<p>
					<a href="mailto@example.com"><s:property value="#u.uemail" /></a>
				</p>
				<address>
					<h3>学&nbsp;&nbsp;&nbsp;校</h3>
					<p>
						<s:property value="#u.uschool" />
					</p>
				</address>

			</div>
			<div class="clearfix"></div>
		</div>
		<!---->
		<link href="css/personinfo/popuo-box.css" rel="stylesheet"
			type="text/css" media="all" />
		<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<!---//pop-up-box---->
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="content">
				<div class="details_header">
					<ul>
						<li><a target="_blank" href="http://localhost:8080/bookcrossing/PersonCenterAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>信息详情</a></li>
						<li><a target="_blank"
							href="http://localhost:8080/bookcrossing/ModPersonCenterAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>修改信息</a></li>
						<li><a target="_blank"
							href="http://localhost:8080/bookcrossing/PersonPublishAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>我的发布</a></li>
						<li><a target="_blank" href="http://localhost:8080/bookcrossing/PersonOrderAction"><span
								class="glyphicon glyphicon-envelope" aria-hidden="true"></span>我的订单</a></li>
					</ul>
				</div>
				<div style="width:100%;">
					<div class="company" style="float:left;width:30%;">
						<h3 class="clr1">个人信息</h3>
						<div class="company_details">
							<h4>
								用户名 <span><s:property value="#u.uname" /></span> </input>
							</h4>
							<h6>User Name</h6>
							<p class="cmpny1"></p>
						</div>
						<div class="company_details">
							<h4>
								昵&nbsp;&nbsp;&nbsp;称 <span><s:property
										value="#u.unickname" /></span>
							</h4>
							<h6>Nick Name</h6>
							<p class="cmpny1"></p>
						</div>
						<div class="company_details">
							<h4>
								邮&nbsp;&nbsp;&nbsp;箱 <span><s:property value="#u.uemail" /></span>
							</h4>
							<h6>Email</h6>
							<p class="cmpny1"></p>
						</div>
						<div class="company_details">
							<h4>
								电&nbsp;&nbsp;&nbsp;话 <span><s:property value="#u.uphone" /></span>
							</h4>
							<h6>Telephone</h6>
							<p class="cmpny1"></p>
						</div>
						<div class="company_details">
							<h4>
								微&nbsp;&nbsp;&nbsp;信 <span><s:property value="#u.uwechat" /></span>
							</h4>
							<h6>WeChat</h6>
							<p class="cmpny1"></p>
						</div>
						<div class="company_details">
							<h4>
								Q&nbsp;&nbsp;&nbsp;Q <span><s:property value="#u.uqq" /></span>
							</h4>
							<h6>QQ Number</h6>
							<p class="cmpny1"></p>
						</div>
						<div class="company_details">
							<h4>
								学&nbsp;&nbsp;&nbsp;校 <span><s:property value="#u.uschool" /></span>
							</h4>
							<h6>School</h6>
							<p class="cmpny1"></p>
						</div>
					</div>
					<div class="company" style="float:left;width:70%;">
						<script>
							var geocoder, map, marker = null;
							var init = function() {
								var center = new qq.maps.LatLng(39.916527,
										116.397128);
								map = new qq.maps.Map(document
										.getElementById('container'), {
									center : center,
									zoom : 15
								});
								//调用地址解析类
								geocoder = new qq.maps.Geocoder({
									complete : function(result) {
										map.setCenter(result.detail.location);
										var marker = new qq.maps.Marker({
											map : map,
											position : result.detail.location
										});
									}
								});
								var address = document
										.getElementById("address").value;
								//通过getLocation();方法获取位置信息值
								geocoder.getLocation(address);
								var cont = document.getElementById('container');
								cont.style.visibility = 'visible';
							}
						</script>
						<input id="address" type="hidden"
							value="<s:property value="#u.uschool" />"> <br />
						<br />
						<br />
						<div id="container" style="width:95%;margin-left:5%;height:400px;"></div>
					</div>
				</div>
			</div>
		</div>
		<!---->
	</s:iterator>
</body>
</html>