/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50615
Source Host           : localhost:3306
Source Database       : bookcrossing

Target Server Type    : MYSQL
Target Server Version : 50615
File Encoding         : 65001

Date: 2016-06-20 12:44:08
*/

CREATE DATABASE bookcrossing;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `book`
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `bId` int(11) NOT NULL AUTO_INCREMENT,
  `uId` int(11) NOT NULL,
  `bName` varchar(30) NOT NULL,
  `bAuthor` varchar(15) NOT NULL,
  `bPublisher` varchar(30) DEFAULT NULL,
  `bType` varchar(30) NOT NULL,
  `bTime` varchar(30) NOT NULL,
  `bPic` varchar(300) NOT NULL,
  `bRequire` int(11) NOT NULL,
  `bStatus` int(11) NOT NULL,
  PRIMARY KEY (`bId`),
  KEY `uId` (`uId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('2', '13', 'Just Enough Research', 'Erika Hall', '', '10-117', '2016-05-12-09-52-45', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('3', '13', 'Content Strategy for Mobile', 'Karen McGrane', '', '10-112', '2016-05-12-09-53-16', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('4', '13', 'Design Is a Job', 'Mike Monteiro', '', '20-215', '2016-05-12-09-53-43', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('5', '13', '移动优先', 'Luke Wroblewski', '', '30-314', '2016-05-12-09-54-06', '1234(2016-06-15-13-57-36).jpg', '1', '1');
INSERT INTO `book` VALUES ('6', '13', 'Designing for Emotion', 'Aarron Walter', '', '20-222', '2016-05-12-09-54-33', '1234(2016-06-15-13-57-36).jpg', '2', '0');
INSERT INTO `book` VALUES ('7', '13', 'Responsive Web Design', 'Ethan Marcotte', '', '20-210', '2016-05-12-09-55-02', '1234(2016-06-15-13-57-36).jpg', '2', '0');
INSERT INTO `book` VALUES ('8', '13', 'CSS3 for Web Designers', 'Dan Cederholm', '', '30-311', '2016-05-12-09-55-27', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('9', '13', 'HTML5 for Web Designers', 'Jeremy Keith', '', '20-217', '2016-05-12-09-55-59', '1234(2016-06-15-13-57-36).jpg', '2', '0');
INSERT INTO `book` VALUES ('10', '13', 'On Web Typography', 'Santa Maria', '', '30-315', '2016-05-12-09-56-44', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('11', '13', 'Responsible Responsive ', 'Scott Jehl', '', '10-114', '2016-05-12-09-57-10', '1234(2016-06-15-13-57-36).jpg', '2', '0');
INSERT INTO `book` VALUES ('12', '13', 'On Web Typography', 'Santa Maria', '', '20-218', '2016-05-12-09-57-41', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('16', '13', '123', '123', '123', '10-110', '2016-05-19-11-23-16', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('17', '13', 'dfas', 'dsfa', 'dksfh', '10-117', '2016-05-19-11-38-52', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('18', '13', '123', '123', '123', '20-219', '2016-06-02-09-06-55', '1234(2016-06-15-13-57-36).jpg', '1', '1');
INSERT INTO `book` VALUES ('19', '11', 'Compilers', 'smith', '机械工业出版社', '1-10', '2016-06-15-13-57-36', '1234(2016-06-15-13-57-36).jpg', '1', '0');
INSERT INTO `book` VALUES ('21', '13', '看见', '柴静', '广西出版社', '9-7', '2016-06-16-10-47-52', 'adlun(2016-06-16-10-47-52).jpg', '1', '1');
INSERT INTO `book` VALUES ('22', '13', 'data', 'dada', 'data', '1-26', '2016-06-19-22-22-16', 'adlun(2016-06-19-22-22-16).jpg', '1', '0');
INSERT INTO `book` VALUES ('23', '13', 'data', 'dada', 'data', '1-1', '2016-06-19-22-24-23', 'adlun(2016-06-19-22-24-23).jpg', '1', '0');
INSERT INTO `book` VALUES ('24', '13', '你的孤独虽败犹荣', '刘同', '机械工业出版社', '9-6', '2016-06-20-08-57-20', 'adlun(2016-06-20-08-57-20).jpg', '1', '0');
INSERT INTO `book` VALUES ('26', '13', 'c language', 'smith', '机械工业出版社', '1-10', '2016-06-20-10-06-52', 'adlun(2016-06-20-10-06-52).png', '1', '1');

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `mId` int(11) NOT NULL AUTO_INCREMENT,
  `uIdSent` int(11) NOT NULL,
  `uIdRece` int(11) NOT NULL,
  `mContent` varchar(100) NOT NULL,
  `mTime` date NOT NULL,
  `mStatus` int(11) NOT NULL,
  PRIMARY KEY (`mId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('2', '13', '11', 'adlun想求购您发布的移动优先书', '2016-06-11', '1');
INSERT INTO `message` VALUES ('3', '13', '11', '2016-06-15-14-02-25时  adlun已同意了您预约的123,他的联系方式为:15201280490', '2016-06-15', '3');
INSERT INTO `message` VALUES ('4', '11', '13', '2016-06-15-14-03-34时  1234已同意了您预约的移动优先,他的联系方式为:12312312312', '2016-06-15', '3');
INSERT INTO `message` VALUES ('6', '13', '14', '2016-06-16-10-54-52时  adlun已同意了您预约的看见,他的联系方式为:15201280490', '2016-06-16', '3');
INSERT INTO `message` VALUES ('8', '20', '13', 'que想求购您发布的《c language》书', '2016-06-20', '1');
INSERT INTO `message` VALUES ('9', '13', '20', '2016-06-20-10-07-44时  adlun已同意了您预约的c language,他的联系方式为:15201280489', '2016-06-20', '3');

-- ----------------------------
-- Table structure for `order`
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `oId` int(11) NOT NULL AUTO_INCREMENT,
  `uIdIn` int(11) NOT NULL,
  `uIdOut` int(11) NOT NULL,
  `bId` int(11) NOT NULL,
  `oTime` date NOT NULL,
  `oStatusIn` int(11) NOT NULL,
  `oStatusOut` int(11) NOT NULL,
  PRIMARY KEY (`oId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('1', '11', '13', '18', '2016-06-02', '2', '1');
INSERT INTO `order` VALUES ('2', '13', '11', '5', '2016-06-11', '2', '1');
INSERT INTO `order` VALUES ('3', '14', '13', '21', '2016-06-16', '2', '1');
INSERT INTO `order` VALUES ('5', '20', '13', '26', '2016-06-20', '2', '1');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uId` int(11) NOT NULL AUTO_INCREMENT,
  `uName` varchar(15) NOT NULL,
  `uNickname` varchar(15) NOT NULL,
  `uPasswd` varchar(32) NOT NULL,
  `uEmail` varchar(20) DEFAULT NULL,
  `uPhone` varchar(15) NOT NULL,
  `uWechat` varchar(15) DEFAULT NULL,
  `uQQ` varchar(15) DEFAULT NULL,
  `uSchool` varchar(30) DEFAULT NULL,
  `uPic` varchar(300) NOT NULL,
  `uFlag` int(11) NOT NULL,
  PRIMARY KEY (`uId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'null', '0', null, null, 'null', 'null', '0');
INSERT INTO `user` VALUES ('13', 'adlun', 'adlun', 'f3d020547a9576fac47bded9ece1f97a', 'adlun@adlun.com', '15201280489', 'adlun', '6666666', '北京交通大学', 'images/header/adlun(2016-06-16-23-13-42).jpg', '1');
INSERT INTO `user` VALUES ('19', 'liang', 'liang', 'ca6b48f157ebb0f3e9c8b04fc52e0494', null, '15202344768', null, null, null, 'images/header/default-header.png', '1');
INSERT INTO `user` VALUES ('20', 'que', 'que', '8cfbcfcd27c86a9ca3648bb0386c654b', null, '13267438642', null, null, null, 'images/header/default-header.png', '1');

-- ----------------------------
-- Table structure for `xhw`
-- ----------------------------
DROP TABLE IF EXISTS `xhw`;
CREATE TABLE `xhw` (
  `xId` int(11) NOT NULL AUTO_INCREMENT,
  `uId` int(11) NOT NULL,
  `xContent` varchar(100) NOT NULL,
  `xTime` date NOT NULL,
  PRIMARY KEY (`xId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xhw
-- ----------------------------
